package ogl.app;

import ogl.vecmath.Color;
import ogl.vecmath.Vector;

//Auxiliary class to represent a single vertex.
public class Vertex {
	public Vector position;
	public Vector normale;
	public Vector texCoord;
	public final Color color;
	
	public Vertex(Vector position, Vector normale, Color color, Vector texCoord) {
		super();
		this.position = position;
		this.normale = normale;
		this.texCoord = texCoord;
		this.color = color;
	}


	

	public Vertex(Vector p, Color c, Vector normale) {
		position = p;
		color = c;
		this.normale = normale;
	}
	
	
	public Vertex(Vector p, Color c) {
		position = p;
		color = c;

	}




	public Vector getTexCoord() {
		return texCoord;
	}




	public Vertex setTexCoord(Vector texCoord) {
		this.texCoord = texCoord;
		return this;
	}
	
	
}
