package ogl.app;


import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL30;

import de.mb.datatypes.MyMathFactory;
import ogl.vecmath.Factory;



//Auxiliary class to represent a Vertex Array Object (VAO)
public class VertexArrayObject{
	// The attribute indices for the vertex data.
	

	private Factory vecmath =  MyMathFactory.vecmath;
	public static int vertexAttribIdx = 0;
	public static int colorAttribIdx = 1;
	public static int normalAttribIdx = 2;
	public static int textureAttribIdx = 3;
	
	// the VAO id
	private int id;
	// the number of stored vertices
	private int numberOfVertices;	
	// the vertex buffer objects associated with this VAO
	private ArrayList<VertexBufferObject> vbos = new ArrayList<VertexBufferObject>();
	
	// Auxiliary class to represent a Vertex Buffer Object (VBO)
	private class VertexBufferObject{
		private final int id;
		private final int attributeIdx;
		
		public VertexBufferObject(int attributeIdx, FloatBuffer data, int vectorsize) {
			this.id  = glGenBuffers();
			this.attributeIdx = attributeIdx;
			
			glBindBuffer(GL_ARRAY_BUFFER, id);
			glBufferData(GL_ARRAY_BUFFER, data, GL_STATIC_DRAW);
			glVertexAttribPointer(attributeIdx, vectorsize, GL_FLOAT, false, 0, 0);
		}
		
		public void enable(){
			glEnableVertexAttribArray(attributeIdx);
		}
		
		public void cleanUp(){
			glDeleteBuffers(id);
		}
	}


	
	public VertexArrayObject(){
		
	}

	
	/**
	 * Creates a vertex array object from the given array of vertices (representing triangles)
	 * @param vertices the vertices to be inserted into the VAO
	 * @return the created vertex array object's id 
	 */
	public VertexArrayObject(List<Vertex> vertices) throws Exception{
		// create the VAO id
		if (OpenGLApp.getGLMajor() >= 3)	
			id = GL30.glGenVertexArrays();
		else 
			id = -1;
		
		// store the number of vertices
		numberOfVertices = vertices.size();

		// Compile vertex data into a Java Buffer data structures that can be
		// passed to the OpenGL API efficiently.
		// create buffers
		FloatBuffer positionData = BufferUtils.createFloatBuffer(numberOfVertices * vecmath.vectorSize());
		FloatBuffer colorData = BufferUtils.createFloatBuffer(numberOfVertices * vecmath.colorSize());
		FloatBuffer normalData = BufferUtils.createFloatBuffer(numberOfVertices * vecmath.vectorSize());
		FloatBuffer textData = BufferUtils.createFloatBuffer(numberOfVertices * 2);//vecmath.textureSize());

		if(vertices.size() > 0){
			if(vertices.get(0).texCoord==null){
				for (Vertex v : vertices) {
					positionData.put(v.position.asArray());
					colorData.put(v.color.asArray());
					normalData.put(v.normale.asArray());
				}
			}else{
				for (Vertex v : vertices) {
					positionData.put(v.position.asArray());
					colorData.put(v.color.asArray());
					normalData.put(v.normale.asArray());
					float[] arr = {v.texCoord.x(), v.texCoord.y()};
					textData.put(arr);
				}
		}
		}
		// fill buffers


		// rewind buffers
		positionData.rewind();
		colorData.rewind();
		normalData.rewind();
		textData.rewind();

		// activate vertex array object
		if (OpenGLApp.getGLMajor() >= 3)
			GL30.glBindVertexArray(id);

		// register Vertex Buffer (VBO)
		vbos.add(new VertexBufferObject(vertexAttribIdx, positionData, 3));

		// register Color Buffer
		vbos.add(new VertexBufferObject(colorAttribIdx, colorData, 3));
		
		// register Normal Buffer
		vbos.add(new VertexBufferObject(normalAttribIdx, normalData, 3));
		
		vbos.add(new VertexBufferObject(textureAttribIdx, textData, 2));
		
		
	}
	



	public void unbind() {
		// Restore state
		glDisableVertexAttribArray(0);
		if (OpenGLApp.getGLMajor() >= 3)
			GL30.glBindVertexArray(0);
	}

	/**
	 * delete allocated buffers and this VAO
	 */
	public void cleanUp(){
		// unbind this VAO
		unbind();
		// delete all VBOs
		for (VertexBufferObject vbo : vbos)
			vbo.cleanUp();
		
		// delete this VAO
		if (OpenGLApp.getGLMajor() >= 3)
			GL30.glDeleteVertexArrays(id);
	}

	/**
	 * activate this VAO
	 */
	public void bind(){
		// activate this VAO
		if (OpenGLApp.getGLMajor() >= 3)
			GL30.glBindVertexArray(id);
		
		// enable all related VBOs
		for (VertexBufferObject vbo : vbos)
			vbo.enable();					
	}
	
	/**
	 * draw this VAO
	 */
	public void draw(){
		
		glDrawArrays(GL_TRIANGLES, 0, numberOfVertices);
	}
	
}
