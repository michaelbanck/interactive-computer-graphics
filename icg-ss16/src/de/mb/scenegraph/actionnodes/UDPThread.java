package de.mb.scenegraph.actionnodes;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import de.mb.datatypes.VectorImpl;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class UDPThread extends Thread {

	private DatagramSocket datagramSocket;
	private int port = 54321;
	private boolean run = true;
	private VectorImpl moveVector;
	private ActionNode callBack;
	
	public UDPThread(VectorImpl moveVector){
		
		this.moveVector = moveVector;
		init();

	}
	
	
	public UDPThread(VectorImpl moveVector, ActionNode callBack){
		
		this.moveVector = moveVector;
		this.callBack = callBack;
		init();

	}
	
	@Override
	public void run() {
		super.run();
		run=true;
		init();
		
		try {
			System.out.println("Wait for Message");
			long time = System.currentTimeMillis();
			while (run) {

				DatagramPacket packet = new DatagramPacket(new byte[12], 0, 12);
				datagramSocket.receive(packet);
				byte[] bytes = packet.getData();
				float x = ByteBuffer.wrap(bytes, 0, 4).order(ByteOrder.BIG_ENDIAN).getFloat();
				float y = ByteBuffer.wrap(bytes, 4, 4).order(ByteOrder.BIG_ENDIAN).getFloat();
				byte calibrate = ByteBuffer.wrap(bytes, 8, 1).get();
				byte speed = ByteBuffer.wrap(bytes, 9, 1).get();

				if (calibrate == 1) {
					moveVector.x = 0;
					moveVector.y = 0;
				}

				long delta = System.currentTimeMillis() - time;

				if (delta > 1000)
					delta = 0;

				float min = 0.01f;

				if (x > min || x < -min)
					moveVector.y += x * delta*2;

				if (y > min || y < -min)
					moveVector.x += y * delta*2;

				time = System.currentTimeMillis();
				
				if(callBack != null){
					callBack.setSpeed(speed);
				}

			}

		} catch (Exception e) {
			if(!(e instanceof SocketException))
				e.printStackTrace();
		}

		System.out.println("Connection End");

	}
	
	private void init(){
		while(datagramSocket == null){
			try{
				datagramSocket = new DatagramSocket(port);
				System.out.println("IP:" + InetAddress.getLocalHost());
				System.out.println("PORT:" + port);
			}catch(IOException e){
				//port is already in use
				port++;
			}
		}
	}
	
	@Override
	public void interrupt() {
		if(run){
			run = false;
			datagramSocket.close();
			datagramSocket = null;
			super.interrupt();
		}
	}
}
