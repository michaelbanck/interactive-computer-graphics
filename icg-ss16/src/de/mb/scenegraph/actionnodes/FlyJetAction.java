package de.mb.scenegraph.actionnodes;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_F6;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_F7;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_F8;

import de.mb.datatypes.MyMathFactory;
import de.mb.datatypes.VectorImpl;
import de.mb.scenegraph.nodes.Node;
import ogl.app.Input;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;

//http://www.lloydgoodall.com/tutorials/first-person-camera-control-with-lwjgl/
/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class FlyJetAction extends ActionNode {


	protected Factory myFactory = MyMathFactory.vecmath;
	protected VectorImpl position = new VectorImpl(0, 0, 0);
	protected VectorImpl smVector = new VectorImpl(0, 0, 0);
	protected Matrix startMatrix;
	protected transient UDPThread udpThread;
	protected transient UDPThread udpThread2;

	float sin = (float) Math.sin(-Math.PI/2.0);
	float cos = (float) Math.cos(-Math.PI/2.0);
	

	public FlyJetAction() {
		super(0);

	}

	public void walkX(float distance) {
		position.x -= distance * sin;
		position.z += distance * cos;
	}

	@Override
	public void simulate(float elapsed, Input input, Node node) {

		if (input.isKeyToggled(GLFW_KEY_F6)) 
			udpThread = smartphoneControll(elapsed,0,-1, udpThread);
		else
			udpThread = stopSmartphoneControll(udpThread);

		if(input.isKeyToggled(GLFW_KEY_F7))
			udpThread2 = smartphoneControll(elapsed,1,0, udpThread2);
		else
			udpThread2 = stopSmartphoneControll(udpThread2);
		
		if(input.isKeyDown(GLFW_KEY_F8))
			reset();


		if (startMatrix == null) {
			startMatrix = node.matrix;
		}

		node.matrix = startMatrix.mult(myFactory.translationMatrix(position));

	}

	
	private void reset() {
		speed = 0;
		position = new VectorImpl(0, 0, 0);
		startMatrix = myFactory.identityMatrix();
		smVector = new VectorImpl(0, 0, 0);

		
	}

	private UDPThread smartphoneControll(float elapsed, float x, float y, UDPThread thread) {
		
	
		rotate(0,0,1, smVector.y*0.0002f);
		rotate(x,y,0, smVector.x*0.0002f);
		

		walkX(speed*elapsed*0.2f);
		
		if (thread == null) {// start smartphone controll thread
			thread = new UDPThread(smVector, this);
			thread.start();
		}
		
		return thread;

	}

	private UDPThread stopSmartphoneControll(UDPThread thread) {
		if (thread != null) {
			thread.interrupt();	
			thread = null;
		}
		
		return null;
	}
	
	
	private void rotate(float x, float y, float z, float value){
		Matrix tran = myFactory.translationMatrix(position);
		Matrix rot = myFactory.rotationMatrix(new VectorImpl(x, y, z), value);
		startMatrix = startMatrix.mult(tran).mult(rot).mult(tran.invertFull());

	}



}
