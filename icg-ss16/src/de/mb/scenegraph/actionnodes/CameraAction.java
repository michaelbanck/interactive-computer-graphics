package de.mb.scenegraph.actionnodes;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_1;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_2;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_F1;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_F2;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_PAGE_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_PAGE_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;

import org.lwjgl.glfw.GLFW;

import de.mb.datatypes.MyMathFactory;
import de.mb.datatypes.VectorImpl;
import de.mb.main.MyOpenGLApp;
import de.mb.scenegraph.nodes.CamNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;
import de.mb.scenegraph.traverser.TraverserMouseClick;
import ogl.app.Input;
import ogl.vecmath.Matrix;

//http://www.lloydgoodall.com/tutorials/first-person-camera-control-with-lwjgl/

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class CameraAction extends ActionNode {

	private MyMathFactory myFactory = MyMathFactory.vecmath2;
	private VectorImpl position = null;
	private final float PI_HALF = (float) (Math.PI / 2.0f);
	private float yaw = 0;
	private float pitch = 0;
	private Matrix startMatrix;
	private boolean freefight = false;
	private boolean setCursor = false;
	private float xOld = 0;
	private float yOld = 0;
	private long time = System.currentTimeMillis();
	private transient Node root;

	

	public CameraAction() {
		super(1);
		position = new VectorImpl(0, 0, 0);

	}
	


	public void yaw(float amount) {
		yaw += amount;
	}

	public void pitch(float amount) {
		pitch += amount;
	}

	private void walk(float distance, float angle) {
		position.x -= distance * (float) Math.sin(yaw + angle);
		position.z += distance * (float) Math.cos(yaw + angle);
		
		if(freefight && angle==0)
			position.y += distance * (float) Math.sin(pitch);
	}

	private void walkZ(float distance) {
		walk(distance, 0);
	}

	private void walkX(float distance) {
		walk(distance, -PI_HALF);
	}


	
	@Override
	public void simulate(float elapsed, Input input, Node node) {
		
		if(startMatrix == null){
			startMatrix = node.matrix;
		}
		
		CamNode cN = (CamNode) node;
		if (cN.getCamNumber() == CamNode.getActive()) {

			if (input.isKeyDown(GLFW_KEY_D))
				walkX(-elapsed * speed);

			if (input.isKeyDown(GLFW_KEY_A))
				walkX(elapsed * speed);

			if (input.isKeyDown(GLFW_KEY_W))
				walkZ(elapsed * speed);

			if (input.isKeyDown(GLFW_KEY_S))
				walkZ(-elapsed * speed);

			if (input.isKeyDown(GLFW_KEY_LEFT))
				yaw(-elapsed * speed);

			if (input.isKeyDown(GLFW_KEY_RIGHT))
				yaw(elapsed * speed);

			if (input.isKeyDown(GLFW_KEY_UP))
				pitch(-elapsed * speed);

			if (input.isKeyDown(GLFW_KEY_DOWN))
				pitch(elapsed * speed);

			if (input.isKeyDown(GLFW_KEY_1)) {
				speed -= elapsed * 10;
				if (speed < 0)
					speed = 0;
			}

			if (input.isKeyDown(GLFW_KEY_2)) {
				speed += elapsed * 10;
			}

			
			if (input.isKeyDown(GLFW_KEY_PAGE_DOWN)) {
				position.y += speed * elapsed;
			}

			if (input.isKeyDown(GLFW_KEY_PAGE_UP)) {
				position.y -= speed * elapsed;
			}
			
			
			if(input.isKeyToggled(GLFW_KEY_F2))
				freefight = true;
			else
				freefight = false;
			
			
			
			if(input.isKeyToggled(GLFW_KEY_F1)){
				cursorControll(input, elapsed);
				
				if(input.isButtonDown(0))
					fireRay();
				
			}else{
				if(setCursor){
					GLFW.glfwSetInputMode(MyOpenGLApp.window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL);
					setCursor = false;
				}
			}
			
			
			node.matrix = lookThrough();
		}
		
	}
	

	private void fireRay() {
		
		if(System.currentTimeMillis() - time < 200)
			return;
			
		if(root == null)
			root = NodeBuilder.getInstance().getRoot();
			
		
		time = System.currentTimeMillis();
		
		float x = 100 * (float) Math.sin(yaw)+position.x;
		float z = -100 * (float) Math.cos(yaw)+position.z;
		float y = -100 * (float) Math.sin(pitch)+position.y;

		root.accept(new TraverserMouseClick(position.mult(-1), new VectorImpl(x,y,z)));
		
	}
	
	
	private void cursorControll(Input input, float elapsed) {
		
		float x = input.getMousePosition().x();
		float y = input.getMousePosition().y();
		
		float xDif = x - xOld;
		float yDif = y - yOld;
		
		xOld = x;
		yOld = y;
		
		
		if(!setCursor){
			GLFW.glfwSetInputMode(MyOpenGLApp.window, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED);
			xDif = 0;
			yDif = 0;
			setCursor = true;
		}
		
		yaw(elapsed * xDif*0.1f);
		pitch(elapsed * yDif*0.1f);
		
	}

	private Matrix lookThrough() {

		Matrix viewMatrix = startMatrix
				.mult(myFactory.rotationMatrixUseRad(new VectorImpl(1, 0, 0), pitch))
				.mult(myFactory.rotationMatrixUseRad(new VectorImpl(0, 1, 0), yaw))
				.mult(myFactory.translationMatrix(position));

		return viewMatrix;
	}
}
