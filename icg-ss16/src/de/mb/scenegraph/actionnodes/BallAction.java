package de.mb.scenegraph.actionnodes;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_F5;
import java.util.ArrayList;
import java.util.List;
import de.mb.datatypes.MyMathFactory;
import de.mb.datatypes.VectorImpl;
import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.TextureManagment;
import ogl.app.Input;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class BallAction extends ActionNode {

	private Factory myFactory;
	private VectorImpl position = new VectorImpl(0, 0, 0);
	private Matrix startMatrix;
	private float scaleFactor;
	private float areaSize;
	private transient UDPThread udpThread;
	private boolean setNumber = true;
	private List<Vector> colorPositions = new ArrayList<Vector>();
	private List<String> texturePath = new ArrayList<String>();
	private List<Integer> textureInt = new ArrayList<Integer>();

	private VectorImpl smVector = new VectorImpl(0, 0, 0);

	public BallAction(float areaSize, GeoNode ball) {
		super(1);
		this.areaSize = areaSize;
		startMatrix = ball.matrix;
		scaleFactor = startMatrix.asArray()[0];
		smVector = new VectorImpl(0, 0, 0);
		myFactory = MyMathFactory.vecmath;
	}


	// moves the camera forward and backward
	public void walkZ(float distance) {
		position.z += distance;

	}

	// moves the camera at the X-Axis
	public void walkX(float distance) {
		position.x += distance;
	}

	@Override
	public void simulate(float elapsed, Input input, Node node) {

		if (input.isKeyToggled(GLFW_KEY_F5))
			smartphoneControll(elapsed);
		else
			stopSmartphoneControll();

		changeBallColor(node);

		node.matrix = ballMatrix();

	}

	private void smartphoneControll(float elapsed) {
		walkZ(elapsed * speed * smVector.y);
		walkX(elapsed * speed * smVector.x);

		if (udpThread == null) {// start smartphone controll thread
			initColorNumbers();
			udpThread = new UDPThread(smVector);
			udpThread.start();
		}

	}

	private void stopSmartphoneControll() {
		if (udpThread != null) {
			udpThread.interrupt();	
			udpThread = null;
		}
	}

	//if the ball reach a gate, color it
	private void changeBallColor(Node node) {

		for (int i = 0; i < colorPositions.size(); i++) {

			Vector v1 = new VectorImpl(position.x, 0, position.z);
			Vector v2 = new VectorImpl(colorPositions.get(i).x(), 0, colorPositions.get(i).z());
			if (v1.sub(v2).length() < 150) {

				if (node instanceof GeoNode) {
					GeoNode gn = (GeoNode) node;
					gn.setTexturePath(texturePath.get(i));
					gn.setTextureNumber(textureInt.get(i));
				}
			}

		}
	}



	public void addGate(Vector gatePosition, String texture) {
		// float scaleIt = (areaSize) / scaleFactor;
		gatePosition = gatePosition.mult(128);
		colorPositions.add(gatePosition);
		texturePath.add(texture);
		//can't init int yet, shader part is not ready, use 0, init later
		textureInt.add(0);
	}

	private void initColorNumbers() {
		if (setNumber)
			for (int i = 0; i < texturePath.size(); i++) {
				textureInt.set(i, TextureManagment.setTextureNormal(texturePath.get(i)));
			}
		setNumber = false;
	}

	private Matrix ballMatrix() {

		checkPosition();

		//use start matrix, to prevent the echo
		Matrix ballMatrix = startMatrix.mult(myFactory.translationMatrix(position));

		return ballMatrix;
	}

	private void checkPosition() {

		float limit = (areaSize - 2.5f) / scaleFactor;

		position.z = checkValue(position.z, limit);
		position.y = checkValue(position.y, limit);
		
	}
	
	//area has to be a square
	private float checkValue(float a, float limit){	
		if (a > limit) 
			return limit;
		
		if (a < -limit)
			return -limit;
		
		return a;
		
	}


}
