package de.mb.scenegraph.actionnodes;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_R;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_3;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_4;

import de.mb.datatypes.MyMathFactory;
import de.mb.scenegraph.nodes.Node;
import ogl.app.Input;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class RotorAction extends ActionNode{
	Vector rotationVector;
	
	public RotorAction(float speed, Vector rotationVector) {
		super(speed);
		this.rotationVector = rotationVector;
	}



	@Override
	public void simulate(float elapsed, Input input, Node node) {
		
		
		if (input.isKeyToggled(GLFW_KEY_R)){
			Matrix rotationMatrix = MyMathFactory.vecmath.rotationMatrix(rotationVector, elapsed*speed);
			node.setMatrix(node.matrix.mult(rotationMatrix));
		}
		
		
		if(input.isKeyDown(GLFW_KEY_3)){
			System.out.println("Reduce rotation speed");
			speed*=0.9f;
		}
		
		
		if(input.isKeyDown(GLFW_KEY_4)){
			System.out.println("Increas rotation speed");
			speed*=1.1f;
		}
	}
}
