package de.mb.scenegraph.actionnodes;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_T;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_P;

import de.mb.datatypes.MyMathFactory;
import de.mb.datatypes.VectorImpl;
import de.mb.effects.Effects;
import de.mb.scenegraph.nodes.Node;
import ogl.app.Input;
import ogl.vecmath.Matrix;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class SimpleJumperAction extends ActionNode {

	public final static int X_AXSIS = 0;
	public final static int Y_AXSIS = 1;
	public final static int Z_AXSIS = 2;

	private Matrix position = MyMathFactory.vecmath.identityMatrix();
	private VectorImpl translationsVector;
	private float lowerLimit, upperLimit;
	private boolean up = true;
	private int axis;
	private String soundPath;

	public SimpleJumperAction(float speed, int axis, float upperLimit, float lowerLimit) {
		super(speed);

		this.axis = axis;
		this.translationsVector = new VectorImpl(0, 0, 0);
		this.upperLimit = upperLimit;
		this.lowerLimit = lowerLimit;
	}
	
	public SimpleJumperAction setSoundPath(String soundPath){
		this.soundPath = soundPath;
		return this;
	}

	@Override
	public void simulate(float elapsed, Input input, Node node) {

		if (input.isKeyToggled(GLFW_KEY_T)) {

			
			if (up)
				up(elapsed);
			else
				down(elapsed, input);

			Matrix mtrans = MyMathFactory.vecmath.translationMatrix(translationsVector);
			position = position.mult(mtrans);
			node.setMatrix(node.matrix.mult(mtrans));

		}

	}

	private void up(float elapsed) {

		if (getPosition() < upperLimit) {
			getAxis(elapsed*speed);
		} else {
			up = false;
			translationsVector = new VectorImpl(0, 0, 0);
		}
	}

	private void down(float elapsed, Input input) {
	
		if (getPosition() > lowerLimit) {
			multiplyAxis(-elapsed*speed*0.3f);
		} else {
			up = true;
			translationsVector = new VectorImpl(0, 0, 0);
			
			
			if(soundPath != null)
				if(input.isKeyToggled(GLFW_KEY_P)){
					Effects.playSound(soundPath);

				}
		}
	}

	private float multiplyAxis(float mult) {

		switch (axis) {
		case X_AXSIS:
			return translationsVector.x += mult;
		case Y_AXSIS:
			return translationsVector.y += mult;
		case Z_AXSIS:
			return translationsVector.z += mult;
		}

		return -1;

	}
	
	private float getAxis(float mult) {

		switch (axis) {
		case X_AXSIS:
			return translationsVector.x = mult;
		case Y_AXSIS:
			return translationsVector.y = mult;
		case Z_AXSIS:
			return translationsVector.z = mult;
		}

		return -1;

	}

	private float getPosition() {

		switch (axis) {
		case X_AXSIS:
			return position.get(3, 0);
		case Y_AXSIS:
			return position.get(3, 1);
		case Z_AXSIS:
			return position.get(3, 2);
		}

		return -1;

	}

}
