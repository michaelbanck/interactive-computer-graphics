package de.mb.scenegraph.actionnodes;

import de.mb.scenegraph.nodes.Node;
import ogl.app.Input;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 * 08-scene-graph-evolution-160531-presentation.pdf Seite 21/140
 */
public abstract class ActionNode {

	protected float speed;
	
	protected ActionNode(){
		this(0);
	}
	

	public ActionNode(float speed) {
		super();
		this.speed = speed;

	}

	public abstract void simulate(float elapsed, Input input, Node node);
	
	public void setSpeed(float speed){
		this.speed = speed;
	}

}
