package de.mb.scenegraph.traverser;

import java.util.HashMap;
import java.util.List;

import de.mb.models.Model;
import de.mb.models.OBJLoader;
import de.mb.scenegraph.actionnodes.ActionNode;
import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class TraverserLoader extends Traversal{

	private HashMap<String, Model> mapModel = new HashMap<>();
	
	public TraverserLoader(List<ActionNode> list) {
		super();
	}

	public void visitNode(Node n){
		if(n.ID.equals(Node.ROOT_ID))
			NodeBuilder.getInstance().setRoot(n);
		
		for(Node cN:n.childNodes)
			cN.accept(this);
		
		if(n instanceof GeoNode){
			setModel((GeoNode) n);
		}
	}


	private void setModel(GeoNode n) {
		
		Model m = n.getModel();
		
		if(!mapModel.containsKey(m.getName())){
		
			m = OBJLoader.loadModel(m.getName());
			mapModel.put(m.getName(), m);
			
		}
		
		n.setModel(mapModel.get(m.getName()));
		
	}

	
	
}
