package de.mb.scenegraph.traverser;

import de.mb.scenegraph.actionnodes.ActionNode;
import de.mb.scenegraph.nodes.Node;
import ogl.app.Input;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class TraverserActionNodes extends Traversal{

	private float elapsed;
	private Input input;
	
	public TraverserActionNodes(float elapsed, Input input) {
		super();
		this.elapsed = elapsed;
		this.input = input;
		
	}

	@Override
	public void visitNode(Node n) {
		doTheStuff(n);
	}
	
	
	private void doTheStuff(Node n){
		for(Node cN:n.childNodes)
			cN.accept(this);
		
		performActionNodes(n);
	}
	
	private void performActionNodes(Node n){
		for(ActionNode a:n.listActionNodes)
			a.simulate(elapsed, input, n);
	}


	
}
