package de.mb.scenegraph.traverser;

import java.util.ArrayDeque;
import java.util.Deque;

import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.TextureManagment;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class TraverserMouseClick extends Traversal{

	private Deque<Matrix> stack = new ArrayDeque<Matrix>();
	private Vector currentPosition, endRayPosition;

	public TraverserMouseClick(Vector currentPosition, Vector endRayPosition) {
		super();

		this.currentPosition = currentPosition;
		this.endRayPosition = endRayPosition;

		
		
	}

	@Override
	public void visitNode(Node n) {
		doTheStuff(n);
	}
	
	
	private Matrix doTheStuff(Node n){
		if(!stack.isEmpty())
			stack.push(stack.peek().mult(n.matrix));
		else
			stack.push(n.matrix);
		
		for(Node cN:n.childNodes)
			cN.accept(this);
		
		
		return stack.pop();
	}
	
	@Override
	public void visit(GeoNode n) {
		Matrix m = doTheStuff(n);
		
		Vector nodePosition = m.getPosition();
		
		for(float f=0; f<30; f=f+0.5f){
			Vector p2 = currentPosition.add(endRayPosition.mult(f/100.0f));
			float l = nodePosition.sub(p2).length();
			if(l < 0.85f){
				changeColor(n);
				f = 30;
			}
				
		}
		
	}

	private void changeColor(GeoNode n) {
		int i = n.getTextureNumber();
		
		n.setTextureNumber((i+1)%TextureManagment.textureCount());
		
	}
	
	
	


	
}
