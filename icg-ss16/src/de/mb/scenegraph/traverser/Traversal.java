package de.mb.scenegraph.traverser;

import de.mb.scenegraph.nodes.CamNode;
import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.GroupNode;
import de.mb.scenegraph.nodes.LightNode;
import de.mb.scenegraph.nodes.Node;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public abstract class Traversal {


	public abstract void  visitNode(Node n);
	
	
	public void visit(GroupNode n){
		visitNode(n);
	}
	public void visit(GeoNode n){
		visitNode(n);
	}
	public void visit(CamNode n){
		visitNode(n);
	}
	public void visit(LightNode n){
		visitNode(n);
	}
	
	
}
