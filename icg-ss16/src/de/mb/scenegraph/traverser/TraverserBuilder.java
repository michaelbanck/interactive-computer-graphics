package de.mb.scenegraph.traverser;

import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.TextureManagment;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class TraverserBuilder extends Traversal{

	
	
	public TraverserBuilder() {
		super();
		GeoNode.setGLVersion(this);
	}



	@Override
	public void visit(GeoNode n) {
		visitNode(n);
		n.setNormalNumber(TextureManagment.setTextureNormal(n.getNormalPath()));
		n.setTextureNumber(TextureManagment.setTextureNormal(n.getTexturePath()));
		n.setSpecularNumber(TextureManagment.setTextureNormal(n.getSpecularPath()));	
	}
	

	@Override
	public void visitNode(Node n) {
		for(Node cN:n.childNodes)
			cN.accept(this);
	}



	
	
	
}
