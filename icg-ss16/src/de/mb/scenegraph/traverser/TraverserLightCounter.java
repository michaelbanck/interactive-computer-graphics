package de.mb.scenegraph.traverser;

import de.mb.scenegraph.nodes.LightNode;
import de.mb.scenegraph.nodes.Node;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class TraverserLightCounter extends Traversal{


	private int count = 0;

	public TraverserLightCounter() {
		super();

	}



	public void visitNode(Node n){
		recursiv(n);
	}


	
	private void recursiv(Node n){
		for(Node cN:n.childNodes)
			cN.accept(this);
	}



	@Override
	public void visit(LightNode n) {
		recursiv(n);
		count++;
	}

	public int getCount(){
		return count;
	}

	
	
}
