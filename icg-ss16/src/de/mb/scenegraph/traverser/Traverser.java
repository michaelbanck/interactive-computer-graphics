package de.mb.scenegraph.traverser;

import java.util.ArrayDeque;
import java.util.Deque;

import de.mb.main.MyOpenGLApp;
import de.mb.scenegraph.nodes.CamNode;
import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.LightNode;
import de.mb.scenegraph.nodes.Node;
import ogl.vecmath.Matrix;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Traverser extends Traversal{

	private Deque<Matrix> stack;
	private MyOpenGLApp myapp;
	
	
	
	public Traverser() {
		super();
		stack = new ArrayDeque<Matrix>();
	}

	public Traverser setMyOpenGLApp(MyOpenGLApp app){
		this.myapp = app;
		return this;
	}

	
	@Override
	public void visitNode(Node n) {
		isEmpty(n);
		stack.pop();
		
	}
	
	
	@Override
	public void visit(GeoNode n){
		
		isEmpty(n);
		Matrix model = stack.pop();
		
	
		myapp.bindDrawUnbindNode(n, model);
		
		
	}
	
	@Override
	public void visit(CamNode n){

		if(!stack.isEmpty()){
			//typically cam traversal, inverted
			Matrix m = stack.peek().invertFull();
			//sub the translation, before performing the rotation
			Matrix trans = m.getTranslation();
			m = m.mult(trans.invertFull()).mult(n.matrix).mult(trans);
			stack.push(m);
		}
		else
			stack.push(n.matrix);
		
		for(Node cN:n.childNodes)
			cN.accept(this);
		
			
		Matrix m = stack.pop();
		
		if(n.getCamNumber() ==  CamNode.getActive()){
			myapp.setCam(m);
		}
		
	}
	
	
	@Override
	public void visit(LightNode n) {
		isEmpty(n);
		Matrix m = stack.pop();

		myapp.addLight(m);
		
		
	}
	
	private void isEmpty(Node n){
		if(!stack.isEmpty())
			stack.push(stack.peek().mult(n.matrix));
		else
			stack.push(n.matrix);
		
		for(Node cN:n.childNodes)
			cN.accept(this);
		
	}


	


	
	
}
