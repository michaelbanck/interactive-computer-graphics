package de.mb.scenegraph.nodes;

import de.mb.models.Model;
import de.mb.scenegraph.traverser.Traversal;
import de.mb.scenegraph.traverser.TraverserBuilder;
import ogl.app.OpenGLApp;
import ogl.app.VertexArrayObject;
import ogl.vecmath.Matrix;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class GeoNode extends Node {

	public static int NOT_IN_USE = 16;
	protected transient VertexArrayObject vertexArrayObject;

	protected String texturePath;
	protected String normalPath;
	protected String specularPath;
	protected int textureNumber = NOT_IN_USE;
	protected int normalNumber = NOT_IN_USE;
	protected int specularNumber = NOT_IN_USE;
	protected Model model;
	protected boolean isLight = false;
	
	
	

	public static void setGLVersion(TraverserBuilder t){
		if(t != null){
			if(OpenGLApp.getGLMajor() >= 4){
				NOT_IN_USE = 10000;
			}else{
				NOT_IN_USE = 16;
			}
		}
	}
	
	protected GeoNode(Matrix matrix, Model model) {
		super(matrix, "GEO_NODE");
		this.model = model;
	}

	@Override
	public void accept(Traversal t) {
		t.visit(this);
	}


	public void setTextureNumber(int number) {
		textureNumber = number;
	}

	public int getTextureNumber() {
		return textureNumber;
	}

	
	public VertexArrayObject getVertexArrayObject() {
		
		if(vertexArrayObject == null)
			try {
				vertexArrayObject = new VertexArrayObject(model.getVertexList());
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return vertexArrayObject;
	}

	public void setNormalNumber(int normalNumber) {
		this.normalNumber = normalNumber;
	}

	public void setModel(Model m) {
		this.model = m;
	}

	public int getNormalNumber() {
		return normalNumber;
	}

	public Model getModel() {
		return model;
	}

	public String getTexturePath() {
		return texturePath;
	}

	public String getNormalPath() {
		return normalPath;
	}

	public String getSpecularPath() {
		return specularPath;
	}

	public GeoNode setSpecularPath(String specularPath) {
		this.specularPath = specularPath;
		return this;
	}

	public int getSpecularNumber() {
		return specularNumber;
	}

	public void setSpecularNumber(int specularNumber) {
		this.specularNumber = specularNumber;
	}

	public boolean isLight() {
		return isLight;
	}

	public GeoNode setLight(boolean isLight) {
		this.isLight = isLight;
		return this;
	}

	public GeoNode setTexturePath(String texturePath) {
		this.texturePath = texturePath;
		return this;
	}

	public GeoNode setNormalPath(String normalPath) {
		this.normalPath = normalPath;
		return this;
	}




}
