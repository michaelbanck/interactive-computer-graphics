package de.mb.scenegraph.nodes;

import java.util.ArrayList;
import java.util.List;

import de.mb.scenegraph.actionnodes.ActionNode;
import de.mb.scenegraph.traverser.Traversal;
import ogl.vecmath.Matrix;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public abstract class Node {

	public static String ROOT_ID ="GROUP_NODE0";
	protected static int NODE_COUNT = 0; 
	public final List<Node> childNodes = new ArrayList<Node>();
	public Matrix matrix;
	public final String ID;
	protected String name;
	public final List<ActionNode> listActionNodes = new ArrayList<ActionNode>();


	
	protected Node(Matrix matrix, String name) {
		super();
		this.name = name;
		this.matrix = matrix;
		ID = name + NODE_COUNT;
		NODE_COUNT++;
	}

	protected void addChild(Node node){
		childNodes.add(node);
	}
	
	public Matrix getMatrix() {
		return matrix;
	}

	public void setMatrix(Matrix matrix) {
		this.matrix = matrix;
	}

	public Node getChildNode(int i) {
		return childNodes.get(i);
	}
	
	public Matrix getTransformMatrix(){
		return matrix;
	}
	
	public List<Node> getChildNodes() {
		return childNodes;
	}
	
	public String getID() {
		return ID;
	}
	
	public abstract void accept(Traversal t);
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof Node){
			Node node = (Node) obj;
			if(node.matrix.equals(this.matrix) && this.childNodes.size() == node.childNodes.size()){
				
				for(int i=0; i<this.childNodes.size(); i++)
					if(node.childNodes.get(i).equals(this.childNodes.get(i)))
						return false;
					
					
				return true;
				
				
			}
			
		}
		
		return false;
	}
	
	public Node addActionNode(ActionNode an){
		this.listActionNodes.add(an);
		return this;
	}
	
	public Node addActionNode(List<ActionNode> list){
		this.listActionNodes.addAll(list);
		return this;
	}
	
}
