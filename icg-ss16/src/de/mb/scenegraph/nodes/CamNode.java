package de.mb.scenegraph.nodes;

import de.mb.datatypes.MyMathFactory;
import de.mb.scenegraph.traverser.Traversal;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class CamNode extends Node {

	
	private static int cam_count = 0;
	private static int active = 0;
	private final int camNumber;

	protected CamNode() {
		super(MyMathFactory.vecmath.identityMatrix(), "CAM_NODE");	
		
		camNumber = cam_count++;
	}

	@Override
	public void accept(Traversal t) {
		t.visit(this);
	}


	
	public static int getActive() {
		return active;
	}

	public static void setActive(int active) {
		CamNode.active = active;
	}

	public static int getCam_count() {
		return cam_count;
	}

	public int getCamNumber() {
		return camNumber;
	}




}
