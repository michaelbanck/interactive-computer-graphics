package de.mb.scenegraph.nodes;

import de.mb.scenegraph.traverser.Traversal;
import ogl.vecmath.Matrix;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class LightNode extends Node {


	protected LightNode(Matrix matrix) {
		super(matrix, "LIGHT_NODE");
	}

	public void accept(Traversal t) {
		t.visit(this);
	}

	@Override
	protected void addChild(Node child) {
		// keine Kinder
		throw new UnsupportedOperationException();
	}

}
