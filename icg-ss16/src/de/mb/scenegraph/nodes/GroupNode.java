package de.mb.scenegraph.nodes;

import de.mb.scenegraph.traverser.Traversal;
import ogl.vecmath.Matrix;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class GroupNode extends Node {

	
	protected GroupNode(Matrix matrix) {
		super(matrix, "GROUP_NODE");
	}

	
	@Override
	public void accept(Traversal t) {
		t.visit(this);
		
	}
}
