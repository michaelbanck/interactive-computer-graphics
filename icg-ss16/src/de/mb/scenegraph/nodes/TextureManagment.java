package de.mb.scenegraph.nodes;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL13;

import ogl.app.Texture;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class TextureManagment {

	private static List<String> list = new ArrayList<String>();
	
	private TextureManagment(){
		
	}
	
	
	public static int setTextureNormal(String path) {

		if (path != null) {
			int index = list.indexOf(path);

			if (index == -1) {
					if(list.size() <= GeoNode.NOT_IN_USE){
					list.add(path);
					index = list.indexOf(path);
					File file = new File(path);
					GL13.glActiveTexture(GL13.GL_TEXTURE0 + index);
	
					Texture texture = new Texture(file);
					texture.bind();
				}else{
					return 0;
				}
			}

			return index;
		}

		return GeoNode.NOT_IN_USE;
	}
	
	public static int textureCount() {
		return list.size();
	}
	
}
