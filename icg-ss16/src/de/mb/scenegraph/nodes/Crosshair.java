package de.mb.scenegraph.nodes;

import de.mb.models.OBJLoader;
import de.mb.models.contants.Models;
import de.mb.models.contants.Textures;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Crosshair extends GeoNode {
	
	

	public Crosshair() {
		super(null, OBJLoader.loadModel(Models.CROSSHAIR));
		isLight = true;
		textureNumber = -1;
		
	}



	public int getTextureNumber() {
		if(textureNumber==-1)
			textureNumber = TextureManagment.setTextureNormal(Textures.WHITE);
		return textureNumber;
	}

	


	public int getNormalNumber() {
		return NOT_IN_USE;
	}



	public int getSpecularNumber() {
		return NOT_IN_USE;
	}

	public boolean isLight() {
		return true;
	}

	
	





}
