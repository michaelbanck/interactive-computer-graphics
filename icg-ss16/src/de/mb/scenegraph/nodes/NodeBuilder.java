package de.mb.scenegraph.nodes;

import de.mb.datatypes.MyMathFactory;
import de.mb.models.Model;
import ogl.vecmath.Matrix;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class NodeBuilder {

	
	private static NodeBuilder instance = new NodeBuilder();
	
	private Node root;
	
	private NodeBuilder(){
		
	}
	
	public Node getRoot(){
		if(root == null)
			root = new GroupNode(MyMathFactory.vecmath.identityMatrix());
	
		return root;
	}
	
	
	
	public Node createGroup(Node parentNode, Matrix matrix){
		GroupNode cn = new GroupNode(matrix);
		parentNode.addChild(cn);
		
		return cn;
		
	}
	
	

	public GeoNode createGeoNode(Node parentNode, Matrix matrix, Model model){
		GeoNode gn = new GeoNode(matrix, model);
		parentNode.addChild(gn);
		
		return gn;
	}
	
	

	public CamNode createCamNode(Node parentNode){
		CamNode cn = new CamNode();
		parentNode.addChild(cn);
		
		return cn;
		
	}
	
	
	public LightNode createLightNode(Node parentNode, Matrix matrix){
		LightNode cn = new LightNode(matrix);
		parentNode.addChild(cn);
		
		return cn;
		
	}
	
	public static NodeBuilder getInstance(){
		return instance;
	}
	
	public void setRoot(Node root){
		if(this.root == null)
			this.root = root;
	}
	
}
