package de.mb.Color;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class YUVColorImpl extends RGBColorImpl {


	public YUVColorImpl(float y, float u, float v) {
		super((y+v/0.88f), (y+u/0.49f), ((y-0.3f*(y+u/0.49f)-0.11f*(y+v/0.88f))/0.59f));
	}

	public YUVColorImpl(RGBColorImpl rgbColorImpl) {
		this.r = rgbColorImpl.getR();
		this.g = rgbColorImpl.getG();
		this.b = rgbColorImpl.getB();
	}


	public float getY() {
		return (float) (0.3*r+0.59*g+0.11*b);
	}

	
	public float getU() {
		return (float) (0.49*(b-getY()));
	}

	
	public float getV() {
		return (float) (0.88*(r-getY()));
	}
	

	@Override
	public String toString() {
		return "YUV("+getY()+", "+getU()+", "+getV()+")";
	}

}
