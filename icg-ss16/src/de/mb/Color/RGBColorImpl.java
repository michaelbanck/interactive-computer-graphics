package de.mb.Color;

import java.nio.FloatBuffer;

import ogl.vecmath.Color;

public class RGBColorImpl implements Color {

	protected float r,g,b;
	
	protected RGBColorImpl(){}
	
	public RGBColorImpl(float r, float g, float b) {
		super();
		this.r = r;
		this.g = g;
		this.b = b;
	}

	@Override
	public float getR() {
		return r;
	}

	@Override
	public float getG() {
		return g;
	}

	@Override
	public float getB() {
		return b;
	}

	@Override
	public boolean isBlack() {
		if(r==0 && g==0 && b==0)
			return true;
		
		return false;
	}

	@Override
	public Color add(Color c) {
		return new RGBColorImpl(r+c.getR(), g+c.getG(), b+c.getB());
	}

	@Override
	public Color modulate(float s) {
		return new RGBColorImpl(r*s, g*s, b*s);
	}

	@Override
	public Color modulate(Color c) {
		return new RGBColorImpl(r*c.getR(), g*c.getG(), b*c.getB());
	}

	@Override
	public Color clip() {
		
		float r = 1;
		float g = 1;
		float b = 1;
		
		if(this.r != 1.0f)
			r = this.r%1.0f;
		if(this.g != 1.0f)
			g = this.g%1.0f;
		if(this.b != 1.0f)
			b = this.b%1.0f;
		
		return new RGBColorImpl(r,g,b);
	}

	@Override
	public float[] asArray() {
		float[] array = {r, g, b};
		return array;
	}

	@Override
	public FloatBuffer asBuffer() {
		FloatBuffer fB = FloatBuffer.allocate(3);
		fillBuffer(fB);
		
		return fB;
	}

	@Override
	public void fillBuffer(FloatBuffer fB) {
		fB.put(r);
		fB.put(g);
		fB.put(b);
	}

	@Override
	public int toAwtColor() {
		return new java.awt.Color(r, g, b).getRGB();
	}

	@Override
	public int compareTo(Color c) {
		if(c.getR() != this.r)
			return -1;
		if(c.getG() != this.g)
			return -1;
		if(c.getB() != this.b)
			return -1;
		
		return 1;
	}

	@Override
	public int size() {
		return 3;
	}
	
	@Override
	public String toString() {
		return "RGB("+r+", "+g+", "+b+")";
	}

}
