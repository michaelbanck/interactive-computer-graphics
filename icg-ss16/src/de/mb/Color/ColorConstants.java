package de.mb.Color;
import de.mb.datatypes.MyMathFactory;
import ogl.vecmath.Color;
import ogl.vecmath.Factory;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class ColorConstants {

	private final static Factory FACTORY 	= MyMathFactory.vecmath;
	
	public final static Color BLACK 		= FACTORY.color(0, 0, 0);
	public final static Color WHITE 		= FACTORY.color(1, 1, 1);
	public final static Color RED 			= FACTORY.color(1, 0, 0);
	public final static Color BLUE 			= FACTORY.color(0, 0, 1);
	public final static Color GREEN 		= FACTORY.color(0, 1, 0);
	public final static Color YELLOW 		= FACTORY.color(1, 1, 0);
	public final static Color VIOLET 		= FACTORY.color(1, 0, 1);
	public final static Color ORANGE 		= FACTORY.color(1, 0.5f, 0);

}
