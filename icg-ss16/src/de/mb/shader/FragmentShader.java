package de.mb.shader;

import ogl.app.OpenGLApp;

public class FragmentShader extends AbstractShader{


	public FragmentShader(){
		super();
	}
	
	@Override
	protected String getSource() {
		return OpenGLApp.getGLMajor() >= 4 ? readCodeFile("fragmentShader44.c") : readCodeFile("fragmentShader31.c");
	}

	@Override
	protected int getFragmentType() {
		return org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
	}
	

	
	
}
