/*******************************************************************************
 * Copyright (c) 2013 Henrik Tramberend, Marc Latoschik.
 * All rights reserved.
 *******************************************************************************/
package de.mb.shader;

import static org.lwjgl.opengl.GL20.glUniform3f;

import java.util.List;

import ogl.vecmath.Vector;

/**
 * Encapsulate the transfer of vector values from the OpenGL application to an
 * GLSL shader uniform variable.
 */
public class LightArrayUniform extends UniformArray {

	/**
	 * Create a new named uniform binding for the specified shader program.
	 * 
	 * @param program
	 *            The shader program that this uniform is bound to.
	 * @param name
	 *            The name of the uniform varibale as seen in the shader
	 *            program.
	 */
	public LightArrayUniform(int program, String name, int count) {
		super(program, name, count);
	}

	/**
	 * Transfer a new value to the shader uniform variable.
	 * 
	 * @param v
	 *            The new value for the uniform variable.
	 */
	public void set(List<Vector> list) {

		for (int i = 0; i < list.size(); i++) {
			glUniform3f(location[i], list.get(i).x(), list.get(i).y(), list.get(i).z());
		}
	}

}
