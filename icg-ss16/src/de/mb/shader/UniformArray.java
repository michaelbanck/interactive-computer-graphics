/*******************************************************************************
 * Copyright (c) 2013 Henrik Tramberend, Marc Latoschik.
 * All rights reserved.
 *******************************************************************************/
package de.mb.shader;

import static org.lwjgl.opengl.GL20.glGetUniformLocation;

/**
 * Encapsulate the transfer of (compound) float values from the OpenGL
 * application to an GLSL shader uniform variable.
 */
public abstract class UniformArray {

	protected final int[] location;

	/**
	 * Create a new named uniform binding for the specified shader program.
	 * 
	 * @param program
	 *            The shader program that this uniform is bound to.
	 * @param name
	 *            The name of the uniform varibale as seen in the shader
	 *            program.
	 */
	public UniformArray(int program, String name, int count) {
		location = new int[count];
		for (int i = 0; i < count; i++) {
			location[i] = glGetUniformLocation(program, name +  "["+i+"]" );
			
		}
	}
}
