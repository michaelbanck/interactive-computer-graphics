package de.mb.shader;

import ogl.app.OpenGLApp;

public class VertexShader extends AbstractShader{


	public VertexShader() {
		super();
	}
		
	@Override
	protected String getSource() {
		if (OpenGLApp.getGLMajor() >= 3)
			return readCodeFile("vsSource30.c");
		else 
			return readCodeFile("vsSource21.c");
	}

	@Override
	protected int getFragmentType() {
		return org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
	}
	
	
	
	
	

}
