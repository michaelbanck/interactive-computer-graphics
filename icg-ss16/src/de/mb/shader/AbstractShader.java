package de.mb.shader;

import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glShaderSource;

import java.io.FileReader;
import java.io.IOException;

import ogl.app.Util;

public abstract class AbstractShader {

	private int id;
	
	
	public AbstractShader(){
		
		id = glCreateShader(getFragmentType());
		glShaderSource(id, getSource());
		glCompileShader(id);
		Util.checkCompilation(id);
	}
	

	public int getShaderID(){
		return id;
	}
	
	protected abstract int getFragmentType();
	
	protected abstract String getSource();
	

	
	protected String readCodeFile(String name){
		String line = "";
		try {
		FileReader fr = new FileReader("Shadercode/"+name);
		
		while(fr.ready()){
			line+=(char) fr.read();
		}
				
		fr.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		
		return line;
	}
	
	
}
