package de.mb.test;


import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.Assert;
import org.junit.Test;

import de.mb.datatypes.MyMathFactory;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;
import ogl.vecmathimp.FactoryDefault;

public class FactoryTest {

	private Factory uniImpl = FactoryDefault.vecmath;
	private Factory myImpl = MyMathFactory.vecmath;
	
	@Test
	public void testVector() {
		
		Vector uniVec 	= uniImpl.vector(1, 1, 1);
		Vector myVec 	= myImpl.vector(1, 1, 1);
		Assert.assertArrayEquals(uniVec.asArray(), myVec.asArray(), 0.0f);
		
		
		uniVec 	= uniImpl.vector(-1, 1, 1);
		myVec 	= myImpl.vector(-1, 1, 1);
		Assert.assertArrayEquals(uniVec.asArray(), myVec.asArray(), 0.0f);
		
		uniVec 	= uniImpl.vector(1, -1, 1);
		myVec 	= myImpl.vector(1, -1, 1);
		Assert.assertArrayEquals(uniVec.asArray(), myVec.asArray(), 0.0f);
		
		uniVec 	= uniImpl.vector(-31, 1, 7);
		myVec 	= myImpl.vector(-31, 1, 7);
		Assert.assertArrayEquals(uniVec.asArray(), myVec.asArray(), 0.0f);

		
		uniVec 	= uniImpl.vector(12, 1, 1);
		myVec 	= myImpl.vector(1, 21, 7);
		//wenn Arrays ungleich, Test erfolgreich!!!
		Assert.assertThat(uniVec.asArray(), IsNot.not(IsEqual.equalTo(myVec.asArray())));
		
	}
	
	
	@Test
	public void testGet() {
	
		Matrix uniMat;
		Matrix myMat;
		
		float[] array = new float[]{
				1,2,3,4,
				5,6,7,8,
				9,10,11,12,
				13,14,15,16
		};
		
		uniMat = uniImpl.matrix(array);
		myMat = myImpl.matrix(array);
		


		
		Assert.assertEquals(uniMat.get(1,1), myMat.get(1,1), 0.0);
		Assert.assertEquals(uniMat.get(1,2), myMat.get(1,2), 0.0);
		Assert.assertEquals(uniMat.get(1,3), myMat.get(1,3), 0.0);
		Assert.assertEquals(uniMat.get(2,0), myMat.get(2,0), 0.0);
		Assert.assertEquals(uniMat.get(2,1), myMat.get(2,1), 0.0);
		Assert.assertEquals(uniMat.get(3,1), myMat.get(3,1), 0.0);
		
	
	}
	
	@Test
	public void testMatrix() {
	
		Matrix uniMat;
		Matrix myMat;
		
		float[] array = new float[]{
				1,2,3,4,
				3,1,2,4,
				2,4,6,3,
				1,3,8,9
		};
		
		uniMat = uniImpl.matrix(array);
		myMat = myImpl.matrix(array);
		
		float[] uni = uniMat.asArray();
		float[] my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.0f);
	}
	
	@Test
	public void testLookAtMatrix() {
		
		Matrix uniMat;
		Matrix myMat;
		

		Vector eye 		= uniImpl.vector(12, 7, 8);
		Vector center 	= uniImpl.vector(-1, 3, -21);
		Vector up 		= uniImpl.vector(14, 9, -4);
		
		uniMat 	= uniImpl.lookatMatrix(eye, center, up);
		myMat 	= myImpl.lookatMatrix(eye, center, up);
		
		float[] uni = uniMat.asArray();
		float[] my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.001f);
		
		
		eye 	= uniImpl.vector(122, 7, 8);
		center 	= uniImpl.vector(-121, 23, -21);
		up 		= uniImpl.vector(14, 29, -4);
		
		myMat 	= uniImpl.lookatMatrix(eye, center, up);
		
		//wenn Arrays ungleich, Test erfolgreich!!!
		Assert.assertThat(uniMat.asArray(), IsNot.not(IsEqual.equalTo(myMat.asArray())));
	}
	
	
	
	@Test
	public void testTransformPoint() {
		
		Matrix uniMat;
		Matrix myMat;
		

		Vector eye 		= uniImpl.vector(12, 7, 8);
		Vector center 	= uniImpl.vector(-1, 3, -21);
		Vector up 		= uniImpl.vector(14, 9, -4);
		
		uniMat 	= uniImpl.lookatMatrix(eye, center, up);
		myMat 	= myImpl.lookatMatrix(eye, center, up);
		
		
		Vector vec = uniImpl.vector(0,0,0);
		Vector uniVec = uniMat.transformPoint(vec);
		Vector myVec = myMat.transformPoint(vec);
		
		float[] uni = uniVec.asArray();
		float[] my = myVec.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.001f);
		
		
		
	}
	
	
	
	
	
	@Test
	public void testPerspectiveMatrix() {
		
		Matrix uniMat;
		Matrix myMat;
		
		float fovy 		= 77.8f;
		float aspect 	= 1.81231f;
		float zNear 	= 10.331248f;
		float zFar 		= 38.4128f;
		
		uniMat 	= uniImpl.perspectiveMatrix(fovy, aspect, zNear, zFar);
		myMat 	= myImpl.perspectiveMatrix(fovy, aspect, zNear, zFar);
		
		float[] uni = uniMat.asArray();
		float[] my = myMat.asArray();
		
		Assert.assertArrayEquals(uni,my, 0.05f);
		
		
		
		aspect 	= -1.81231f;
		
		myMat 	= myImpl.perspectiveMatrix(fovy, aspect, zNear, zFar);
		
		
		//wenn Arrays ungleich, Test erfolgreich!!!
		Assert.assertThat(uniMat.asArray(), IsNot.not(IsEqual.equalTo(myMat.asArray())));
	}
	
	
	@Test
	public void testIdentityMatrix() {
		
		Matrix uniMat;
		Matrix myMat;

		
		uniMat 	= uniImpl.identityMatrix();
		myMat 	= myImpl.identityMatrix();


		Assert.assertArrayEquals(uniMat.asArray(), myMat.asArray(), 0.0f);

	
	}
	
	
	@Test
	public void testScaleMatrix() {
		
		Matrix uniMat;
		Matrix myMat;
		
		float x = 1412.53f;
		float y = 83.312f;
		float z = 4124.66f;
		
		uniMat 	= uniImpl.scaleMatrix(x,y,z);
		myMat 	= myImpl.scaleMatrix(x,y,z);
		
		
		float[] uni = uniMat.asArray();
		float[] my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.0f);
		
		
	}
	
	
	@Test
	public void testTranslationMatrix() {
		
		Matrix uniMat;
		Matrix myMat;
		
		float x = 1412.53f;
		float y = 83.312f;
		float z = 4124.66f;
		
		uniMat 	= uniImpl.translationMatrix(x,y,z);
		myMat 	= myImpl.translationMatrix(x,y,z);
		
		
		float[] uni = uniMat.asArray();
		float[] my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.0f);
		
		uniMat 	= uniImpl.translationMatrix(x,y,z).getTranslation();
		myMat 	= myImpl.translationMatrix(x,y,z).getTranslation();
		
		
		
		uni = uniMat.asArray();
		my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.0f);
		
		uniMat 	= uniImpl.translationMatrix(x,y,z).invertFull();
		myMat 	= myImpl.translationMatrix(x,y,z).invertFull();
		
		uni = uniMat.asArray();
		my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.0f);
		
		uniMat 	= uniImpl.translationMatrix(x,y,z).invertFull().transpose();
		myMat 	= myImpl.translationMatrix(x,y,z).invertFull().transpose();
		
		uni = uniMat.asArray();
		my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.0f);
		
		
		uniMat 	= uniImpl.translationMatrix(x,y,z).invertFull().getRotation();
		myMat 	= myImpl.translationMatrix(x,y,z).invertFull().getRotation();
		
		uni = uniMat.asArray();
		my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.0f);
		
		uniMat 	= uniImpl.translationMatrix(x,y,21312234.124f).invertFull().transpose();
		myMat 	= myImpl.translationMatrix(x,y,z).invertFull().transpose();
		
		uni = uniMat.asArray();
		my = myMat.asArray();
		
		//wenn Arrays ungleich, Test erfolgreich!!!
		Assert.assertThat(uniMat.asArray(), IsNot.not(IsEqual.equalTo(myMat.asArray())));
	}
	
	@Test
	public void testRotationMatrix() {
		
		Matrix uniMat;
		Matrix myMat;
		
		float x = 1412.53f;
		float y = 83.312f;
		float z = 4124.66f;
		float angle = 92;
		
		uniMat 	= uniImpl.rotationMatrix(x,y,z, angle);
		myMat 	= myImpl.rotationMatrix(x,y,z, angle);
		
		float[] uni = uniMat.asArray();
		float[] my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.01f);
	
		uniMat 	= uniImpl.rotationMatrix(x,y,z, angle).getRotation();
		myMat 	= myImpl.rotationMatrix(x,y,z, angle).getRotation();
		
		uni = uniMat.asArray();
		my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.01f);
		
		uniMat 	= uniImpl.translationMatrix(x,y,21312234.124f).invertFull().transpose();
		myMat 	= myImpl.translationMatrix(x,y,z).invertFull().transpose();
		
		//wenn Arrays ungleich, Test erfolgreich!!!
		Assert.assertThat(uniMat.asArray(), IsNot.not(IsEqual.equalTo(myMat.asArray())));
	}
	
	
	@Test
	public void testComplexMult() {
		
		Matrix uniMat;
		Matrix myMat;
		
		float[] array = new float[]{
				1,2,3,4,
				3,1,2,4,
				2,4,6,3,
				1,3,8,9
		};
		
		uniMat 	= uniImpl.matrix(array);
		myMat 	= myImpl.matrix(array);
		
		float[] uni = uniMat.asArray();
		float[] my = myMat.asArray();
		
		Assert.assertArrayEquals(uni, my, 0.0f);
		
		uniMat 	= uniImpl.matrix(array).mult(uniMat);
		myMat 	= myImpl.matrix(array).mult(myMat);
		
		
		uni = uniMat.asArray();
		my  = myMat.asArray();
		
		Assert.assertArrayEquals(uni,my, 0.0f);
		
		uniMat 	= uniImpl.matrix(array).mult(uniMat).invertFull();
		myMat 	= myImpl.matrix(array).mult(myMat).invertFull();
		
		uni = uniMat.asArray();
		my  = myMat.asArray();
		
//		Assert.assertArrayEquals(uni,my, 0.02f);
	
	}
	

}
