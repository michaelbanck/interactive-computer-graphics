package de.mb.models;

import java.util.ArrayList;
import java.util.List;

import de.mb.Color.ColorConstants;
import ogl.app.Vertex;
import ogl.vecmath.Color;
import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Model {

	protected final String name;
	public transient List<Vector> vertices = new ArrayList<Vector>();
	public transient List<Vector> texturen = new ArrayList<Vector>();
	public transient List<Vector> normals = new ArrayList<Vector>();
	public transient List<Face> faces = new ArrayList<Face>();
	private transient List<Vertex> vertexList;


	public Color color = ColorConstants.RED;

	
	public Model(String name){
		this.name = name;
	}
	
	public void addNormal(Vector normal) {
		normals.add(normal);
	}

	public void addVertices(Vector vertex) {
		vertices.add(vertex);
	}

	public void addFace(Face face) {
		faces.add(face);
	}

	public void addTexture(Vector tex) {
		texturen.add(tex);
	}

	public List<Vertex> getVertexList() {

		if (vertexList == null) {
			vertexList = new ArrayList<Vertex>();

			if (texturen.size() == 0 && normals.size() == 0) {
				for (Face face : faces) {
					Vector v1 = vertices.get((face.vertex.index0() - 1));

					vertexList.add(new Vertex(v1, color));

					Vector v2 = vertices.get(face.vertex.index1() - 1);

					vertexList.add(new Vertex(v2, color));

					Vector v3 = vertices.get(face.vertex.index2() - 1);

					vertexList.add(new Vertex(v3, color));
				}
			} else if (texturen.size() == 0) {
				for (Face face : faces) {
					Vector v1 = vertices.get(face.vertex.index0() - 1);
					Vector n1 = normals.get(face.normal.index0() - 1);

					vertexList.add(new Vertex(v1, color, n1));

					Vector v2 = vertices.get(face.vertex.index1() - 1);
					Vector n2 = normals.get(face.normal.index1() - 1);

					vertexList.add(new Vertex(v2, color, n2));

					Vector v3 = vertices.get(face.vertex.index2() - 1);
					Vector n3 = normals.get(face.normal.index2() - 1);

					vertexList.add(new Vertex(v3, color, n3));
				}
			} else {

				for (Face face : faces) {
					Vector v1 = vertices.get(face.vertex.index0() - 1);
					Vector n1 = normals.get(face.normal.index0() - 1);
					Vector t1 = texturen.get(face.texturen.index0() - 1);

					vertexList.add(new Vertex(v1, color, n1).setTexCoord(t1));

					Vector v2 = vertices.get(face.vertex.index1() - 1);
					Vector n2 = normals.get(face.normal.index1() - 1);
					Vector t2 = texturen.get(face.texturen.index1() - 1);

					vertexList.add(new Vertex(v2, color, n2).setTexCoord(t2));

					Vector v3 = vertices.get(face.vertex.index2() - 1);
					Vector n3 = normals.get(face.normal.index2() - 1);
					Vector t3 = texturen.get(face.texturen.index2() - 1);

					vertexList.add(new Vertex(v3, color, n3).setTexCoord(t3));
				}
			}
		}
		return vertexList;
	}
	
	public String getName() {
		return name;
	}

}
