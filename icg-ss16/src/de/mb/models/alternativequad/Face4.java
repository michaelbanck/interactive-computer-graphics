package de.mb.models.alternativequad;

import de.mb.datatypes.Vector4Integer;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Face4 {

	public Vector4Integer vertex; 
	public Vector4Integer normal;
	public Vector4Integer texturen;
	
	public Face4(Vector4Integer vertex, Vector4Integer normal, Vector4Integer texturen) {
		super();
		this.vertex = vertex;
		this.normal = normal;
		this.texturen = texturen;
	}

	public Face4(Vector4Integer vertex, Vector4Integer normal) {
		super();
		this.vertex = vertex;
		this.normal = normal;
	}


	
	
	
	
}
