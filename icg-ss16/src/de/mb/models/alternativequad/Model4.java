package de.mb.models.alternativequad;


import java.util.ArrayList;
import java.util.List;

import de.mb.Color.ColorConstants;
import ogl.app.Vertex;
import ogl.vecmath.Color;
import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Model4 {

	public List<Vector> vertices = new ArrayList<Vector>();
	public List<Vector> texturen = new ArrayList<Vector>();
	public List<Vector> normals = new ArrayList<Vector>();
	public List<Face4> faces = new ArrayList<Face4>();
	public Color color = ColorConstants.BLUE;
	
	
	public void addNormal(Vector normal){
		normals.add(normal);
	}
	
	public void addVertices(Vector vertex){
		vertices.add(vertex);
	}
	public void addFace(Face4 face){
		faces.add(face);
	}
	
	public void addTexture(Vector tex){
		texturen.add(tex);
	}
	
	
	
	public List<Vertex> getVertexList(){
		
		
		
		List<Vertex> list = new ArrayList<Vertex>();
		
		if(texturen.size() == 0 && normals.size() == 0){
			for(Face4 face:faces){
				Vector v1 = vertices.get( (face.vertex.i0 -1));
	
				list.add(new Vertex(v1, color));
				
				Vector v2 = vertices.get( face.vertex.i1 -1);
				
				list.add(new Vertex(v2, color));
				
				Vector v3 = vertices.get( face.vertex.i2 -1);
				
				list.add(new Vertex(v3, color));
				
				Vector v4 = vertices.get( face.vertex.i3 -1);
				
				list.add(new Vertex(v4, color));
			}
		}else if(texturen.size() == 0){
			for(Face4 face:faces){
				Vector v1 = vertices.get(  face.vertex.i0 -1);
				Vector n1 = normals.get(  face.normal.i0 -1);
				
				list.add(new Vertex(v1, color, n1));
				
				Vector v2 = vertices.get(  face.vertex.i1 -1);
				Vector n2 = normals.get(  face.normal.i1 -1);
				
				list.add(new Vertex(v2, color, n2));
				
				Vector v3 = vertices.get(  face.vertex.i2 -1);
				Vector n3 = normals.get(  face.normal.i2 -1);
				
				list.add(new Vertex(v3, color, n3));
				
				
				Vector v4 = vertices.get(  face.vertex.i3 -1);
				Vector n4 = normals.get(  face.normal.i3 -1);
				
				list.add(new Vertex(v4, color, n4));
			}
		}else{

			for(Face4 face:faces){
				Vector v1 = vertices.get(  face.vertex.i0 -1);
				Vector n1 = normals.get(  face.normal.i0 -1);
				Vector t1 = texturen.get(  face.texturen.i0 -1);
				
				list.add(new Vertex(v1, color, n1).setTexCoord(t1));
				
				Vector v2 = vertices.get(  face.vertex.i1 -1);
				Vector n2 = normals.get(  face.normal.i1 -1);
				Vector t2 = texturen.get(  face.texturen.i1 -1);
				
				list.add(new Vertex(v2, color, n2).setTexCoord(t2));
				
				Vector v3 = vertices.get(  face.vertex.i2 -1);
				Vector n3 = normals.get(  face.normal.i2 -1);
				Vector t3 = texturen.get(  face.texturen.i2 -1);
				
				list.add(new Vertex(v3, color, n3).setTexCoord(t3));
				
				Vector v4 = vertices.get(  face.vertex.i3 -1);
				Vector n4 = normals.get(  face.normal.i3 -1);
				Vector t4 = texturen.get(  face.texturen.i3 -1);
				
				list.add(new Vertex(v4, color, n4).setTexCoord(t4));
			}
		}
		
		return list;
	}
	
}
