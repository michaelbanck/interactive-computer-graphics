package de.mb.models;

import de.mb.datatypes.Vector3Integer;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Face {

	public Vector3Integer vertex; 
	public Vector3Integer normal;
	public Vector3Integer texturen;
	
	public Face(Vector3Integer vertex, Vector3Integer normal, Vector3Integer texturen) {
		super();
		this.vertex = vertex;
		this.normal = normal;
		this.texturen = texturen;
	}

	public Face(Vector3Integer vertex, Vector3Integer normal) {
		super();
		this.vertex = vertex;
		this.normal = normal;
	}


	
	
	
	
}
