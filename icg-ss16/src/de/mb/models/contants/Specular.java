package de.mb.models.contants;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Specular {

	private static String FOLDER = "specular/";

	public static final String STONE = FOLDER + "smstone_s.png";
	public static final String WALL = FOLDER + "wall_s.jpg";
	public static final String BRICKS = FOLDER + "brick_s.jpg";
	public static final String TILES = FOLDER + "tiles_s.jpg";

	private Specular() {
	}
}
