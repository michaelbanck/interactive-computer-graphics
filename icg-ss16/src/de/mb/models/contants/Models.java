package de.mb.models.contants;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Models {

	private static final String FOLDER = "models/";
	public static final String BUNNY = FOLDER + "bunny.obj";
	public static final String CUBE = FOLDER + "cube.obj";
	public static final String F16 = FOLDER + "f16.obj";
	public static final String NABOO = FOLDER + "naboo.obj";
	public static final String GROUND = FOLDER + "ground.obj";
	public static final String CUBE_CENTERED = FOLDER + "cube2.obj";
	public static final String CUBE3 = FOLDER + "cube3.obj";
	public static final String CROSSHAIR = FOLDER + "crosshair.obj";;

	private Models() {
	}
}
