package de.mb.models.contants;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Normals {

	private static final String FOLDER = "normals/";

	public static final String RIFFEL = FOLDER + "riffelnormal.jpg";
	public static final String WALL = FOLDER + "wall_normal.jpg";
	public static final String ROCK = FOLDER + "rock_n.png";
	public static final String BRICKS = FOLDER + "bricks_n.jpg";
	public static final String WOOD = FOLDER + "wood_n.jpg";
	public static final String EARTH = FOLDER + "earth_normal_map.jpg";
	public static final String MERCURY = FOLDER + "mercury_n.jpg";
	public static final String VENUS = FOLDER + "venus_n.jpg";
	public static final String MARS = FOLDER + "mars_n.jpg";
	public static final String NEPTUNE = FOLDER + "neptune_n.jpg";
	public static final String JUPITER = FOLDER + "jupiter_n.jpg";
	public static final String SATURN = FOLDER + "saturn_n.jpg";
	public static final String URANUS = FOLDER + "uranus_n.jpg";
	public static final String SUN = FOLDER + "sun_n.jpg";
	public static final String MOON = FOLDER + "moon_n.jpg";
	public static final String STONE = FOLDER + "smstone_n.png";
	public static final String TILES = FOLDER + "tiles_n.jpg";

	private Normals() {
	}
}
