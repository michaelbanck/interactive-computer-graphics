package de.mb.models.contants;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Textures {


	private static final String FOLDER = "texturen/";

	public static final String SCHACH = FOLDER + "schach.jpg";
	public static final String F16s = FOLDER + "f16s2.png";
	public static final String RED = FOLDER + "red.jpg";
	public static final String STONE = FOLDER + "smstone.png";
	public static final String MOON = FOLDER + "moon.jpg";
	public static final String GROUND = FOLDER + "groundsmall.jpg";
	public static final String RIFFEL = FOLDER + "riffel.jpg";
	public static final String WALL = FOLDER + "wall.jpg";
	public static final String ROCK = FOLDER + "rock.png";
	public static final String BRICKS = FOLDER + "brick.jpg";
	public static final String GRAS = FOLDER + "gras.jpg";
	public static final String WOOD = FOLDER + "wood.jpg";
	public static final String MERCURY = FOLDER + "2k_mercury.jpg";
	public static final String VENUS = FOLDER + "2k_venus_surface.jpg";
	public static final String MARS = FOLDER + "2k_mars.jpg";
	public static final String NEPTUNE = FOLDER + "2k_neptune.jpg";
	public static final String JUPITER = FOLDER + "2k_jupiter.jpg";
	public static final String SATURN = FOLDER + "2k_saturn.jpg";
	public static final String URANUS = FOLDER + "2k_uranus.jpg";
	public static final String SUN = FOLDER + "2k_sun.jpg";
	public static final String EARTH = FOLDER + "earth_daymap.jpg";
	public static final String WHITE = FOLDER + "white.jpg";
	public static final String CASTLE = FOLDER + "castle.jpg";
	public static final String TILES = FOLDER + "tiles.jpg";
	public static final String BLUE = FOLDER + "blue.jpg";
	public static final String GREEN = FOLDER + "green.jpg";
	public static final String GRAY = FOLDER + "gray.jpg";
	public static final String SPACE = FOLDER + "space.jpg";

	private Textures() {
	}
}
