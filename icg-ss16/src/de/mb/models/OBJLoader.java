package de.mb.models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import de.mb.datatypes.Vector3Integer;
import de.mb.datatypes.VectorImpl;
import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 * https://www.youtube.com/watch?v=izKAvSV3qk0&feature=youtu.be
 *
 * @author Michael Banck 2004907
 *
 */
public class OBJLoader {

	private final static String VECTOR = "v ";
	private final static String NORMAL = "vn";
	private final static String TEXTURE = "vt";
	private final static String FACE = "f ";

	public static Model loadModel(String fileName) {

		try {
			File f = new File(fileName);

			BufferedReader reader = new BufferedReader(new FileReader(f));
			Model m = new Model(fileName);

			for (String line = ""; line != null; line = reader.readLine()) {
				line = line.trim().replaceAll(" +", " ");
				if (line.startsWith(VECTOR))
					m.addVertices(getVector(line));
				else if (line.startsWith(NORMAL))
					m.addNormal(getVector(line));
				else if (line.startsWith(FACE))
					m.addFace(getFace(line));
				else if (line.startsWith(TEXTURE))
					m.addTexture(getTexture(line));
			}

			reader.close();
			return m;

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	private static Vector getTexture(String line) {
		String[] lineParts = line.split(" ");

		float x = Float.valueOf(lineParts[1]);
		float y = Float.valueOf(lineParts[2]);

		return new VectorImpl(x, y, 0.0f);
	}

	private static Vector getVector(String line) {
		String[] lineParts = line.split(" ");

		float x = Float.valueOf(lineParts[1]);
		float y = Float.valueOf(lineParts[2]);
		float z = Float.valueOf(lineParts[3]);

		return new VectorImpl(x, y, z);
	}

	private static Face getFace(String line) {
		String[] lineParts = line.split(" ");

		String splitter = getSplitter(line);

		Vector3Integer vertexIndices = getVector(lineParts, splitter, 0);
		Vector3Integer normalIndices = getVector(lineParts, splitter, 2);
		Vector3Integer textureIndices = getVector(lineParts, splitter, 1);

		return new Face(vertexIndices, normalIndices, textureIndices);
	}

	private static Vector3Integer getVector(String[] lineParts, String splitter, int index) {

		if (lineParts[1].split(splitter).length > index) {

			int x = Integer.valueOf(lineParts[1].split(splitter)[index]);
			int y = Integer.valueOf(lineParts[2].split(splitter)[index]);
			int z = Integer.valueOf(lineParts[3].split(splitter)[index]);

			return new Vector3Integer(x, y, z);
		}
		return null;
	}

	private static String getSplitter(String line) {
		if (line.contains("//"))
			return "//";

		return "/";
	}

}
