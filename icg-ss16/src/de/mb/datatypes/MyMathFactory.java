package de.mb.datatypes;

import de.mb.Color.RGBColorImpl;
import ogl.vecmath.Color;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class MyMathFactory implements Factory{
	
//testen
//	public final static Factory vecmath = FactoryDefault.vecmath;
	public final static Factory vecmath = new MyMathFactory();
	public final static MyMathFactory vecmath2 = new MyMathFactory();

	
	private MyMathFactory(){}
	
	/**
	 * creates a new vector
	 * @param x the vector's x component
	 * @param y the vector's y component
	 * @param z the vector's z component
	 * @return the created vector
	 */
	public Vector vector(float x, float y, float z){
		return new VectorImpl(x, y, z);
	}
	
	/**
	 * creates a new color
	 * @param r the color's red component
	 * @param g the color's green component
	 * @param b the color's blue component
	 * @return the created vector
	 */
	public Color color(float r, float g, float b){
		return new RGBColorImpl(r, g, b);
		
	}

	@Override
	public Vector xAxis() {
		return vector(1, 0, 0);
	}

	@Override
	public Vector yAxis() {
		return vector(0, 1, 0);
	}

	@Override
	public Vector zAxis() {
		return vector(0, 0, 1);
	}

	@Override
	public int vectorSize() {
		return 3;
	}

	public int textureSize() {
		return 2;
	}

	
	@Override
	public Matrix identityMatrix() {

		return new MatrixIdentity();
	}

	@Override
	public Matrix matrix(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13,
			float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33) {
		
		return new MatrixImpl(new float[] {
				m00, m01, m02, m03,
				m10, m11, m12, m13,
				m20, m21, m22, m23,
				m30, m31, m32, m33
				
		});
	}

	@Override
	public Matrix matrix(float[] elements) {
		return new MatrixImpl(elements);
	}

	@Override
	public Matrix matrix(Vector b0, Vector b1, Vector b2) {
		float [] array = {
				b0.x(), b1.x(), b2.x(), 0,
				b0.y(), b1.y(), b2.y(), 0,
				b0.z(), b1.z(), b2.z(), 0,
				0, 0, 0, 0
				
		};
		return new MatrixImpl(array);
	}

	@Override
	public Matrix translationMatrix(float x, float y, float z) {
		float [] array = {
				1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				x,y,z,1,
				
		};
		
		
		float [] iArray = {
				1,0,0,0,
				0,1,0,0,
				0,0,1,0,
				-x,-y,-z,1,
				
		};
		
		return new MatrixImpl(array, iArray);
	}
	
	/**
	 * returns a matrix that translates a point by the given vector
	 * @param translation the translation vector
	 * @return a matrix that translates a point by the given vector
	 */
	public Matrix translationMatrix(Vector t){
			return translationMatrix(t.x(), t.y(), t.z());
	}
	

	@Override
	public Matrix rotationMatrix(float ax, float ay, float az, float angle) {
		return new MatrixImpl(getUniversalRotation(ax, ay, az, angle), getUniversalRotation(ax, ay, az, -angle));
	}

	
	
	/**
	 * returns a rotation matrix that represents the rotation defined by the given axis and angle
	 * @param axis the rotation axis
	 * @param angleInDegrees the rotation angle in degrees
	 * @return a rotation matrix that represents the rotation defined by the given axis and angle
	 */
	public Matrix rotationMatrix(Vector v, float angle) {
		return new MatrixImpl(getUniversalRotation(v.x(), v.y(), v.z(), angle), getUniversalRotation(v.x(), v.y(), v.z(), -angle));
	}

	
	public Matrix rotationMatrixUseRad(float ax, float ay, float az, float angle){
		return rotationMatrix(new VectorImpl(ax, ay, az), angle);
	}
	
	public Matrix rotationMatrixUseRad(Vector v, float angleInRad){
		return new MatrixImpl(getUniversalRotation(v, angleInRad), getUniversalRotation(v, -angleInRad));
	}
	
	private float[] getUniversalRotation(float n1, float n2, float n3, float a){
		Vector v = new VectorImpl(n1, n2, n3);
		return getUniversalRotation(v, (float) Math.toRadians(a));
	}
	
	private float[] getUniversalRotation(Vector v, float angleInRad){
		
		v = v.normalize();
		float n1 = v.x();
		float n2 = v.y();
		float n3 = v.z();
		
		
		float cos 	= (float) Math.cos(angleInRad);
		float cosI	= (1.0f-cos);
		float sin	= (float) Math.sin(angleInRad);
		

		return new float[] {
				n1*n1*cosI+cos,		n2*n1*cosI+n3*sin,		n3*n1*cosI-n2*sin,	0,
				n1*n2*cosI-n3*sin,	n2*n2*cosI+cos,			n3*n2*cosI+n1*sin,	0,
				n1*n3*cosI+n2*sin,	n2*n3*cosI-n1*sin,		n3*n3*cosI+cos,		0,
				0,					0,						0,					1
		};
		
	}
	
	@Override
	public Matrix scaleMatrix(Vector s) {
		return scaleMatrix(s.x(), s.y(), s.z());
	}

	@Override
	public Matrix scaleMatrix(float x, float y, float z) {
		
		float[] s = {
				x,0,0,0,
				0,y,0,0,
				0,0,z,0,
				0,0,0,1
		};
		
		float[] sI = {
				1.0f/x,0,0,0,
				0,1.0f/y,0,0,
				0,0,1.0f/z,0,
				0,0,0,1
		};
		
		return new MatrixImpl(s, sI);
	}

	@Override
	public Matrix lookatMatrix(Vector eye, Vector center, Vector up) {
		//https://msdn.microsoft.com/de-de/library/windows/desktop/bb281710(v=vs.85).aspx
		Vector zAxis = eye.sub(center);	
		zAxis = zAxis.normalize();
		Vector xAxis = up.cross(zAxis);	
		xAxis = xAxis.normalize();
		Vector yAxis = zAxis.cross(xAxis);	
		

		float [] array = {
				xAxis.x(), 			yAxis.x(),	 		zAxis.x(), 			0,
				xAxis.y(), 			yAxis.y(), 			zAxis.y(), 			0,
				xAxis.z(), 			yAxis.z(), 			zAxis.z(), 			0,
				-xAxis.dot(eye), 	-yAxis.dot(eye), 	-zAxis.dot(eye), 	1
				
		};
		
		return new MatrixImpl(array);
		
		
	}

	@Override
	public Matrix frustumMatrix(float left, float right, float bottom, float top, float zNear, float zFar) {
		//not used
		throw new UnsupportedOperationException("NOT IMPLEMENTED");
	}

	@Override
	public Matrix perspectiveMatrix(float fovy, float aspect, float zNear, float zFar) {
	//http://www.mathematik.uni-marburg.de/~thormae/lectures/graphics1/graphics_6_1_ger_web.html#11
		
		float alpha = (zFar + zNear)/(zNear-zFar);
		float beta 	= (2*zFar*zNear)/(zNear-zFar);
		
		float rads = (float) Math.toRadians(0.5*fovy);
		
		//coTan
		float f = (float) (1.0/Math.tan(rads));
		
		float[] s = {
				f/aspect,0,0,0,
				0,f,0,0,
				0,0,alpha,-1,
				0,0,beta,0
		};
		
		return new MatrixImpl(s);
	}

	@Override
	public int colorSize() {
		return 3;
	}
}
