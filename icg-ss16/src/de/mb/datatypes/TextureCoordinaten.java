package de.mb.datatypes;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class TextureCoordinaten {

	private float x;
	private float y;
	
	
	public TextureCoordinaten(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	public float[] asArray(){
		return new float[]{
				x,y
		};
	}
	
}
