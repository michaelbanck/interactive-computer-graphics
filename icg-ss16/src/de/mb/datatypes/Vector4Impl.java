	package de.mb.datatypes;

import java.nio.FloatBuffer;
import java.util.Arrays;

import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Vector4Impl implements Vector{

	public float x,y,z,a;
	
	public Vector4Impl(){
		super();
	}
	
	public Vector4Impl(float x, float y, float z, float a) {
		this();

		this.x = x;
		this.y = y;
		this.z = z;
		this.a = a;

	}
	
	public Vector4Impl(float[] values) {
		this(values[0],values[1],values[2], values[3]);
	}
	
	public void setX(float x){
		this.x = x;
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public void setZ(float z){
		this.z = z;
	}
	
	public void setA(float a){
		this.a = a;
	}

	@Override
	public float x() {
		return x;
	}

	@Override
	public float y() {
		return y;
	}

	@Override
	public float z() {
		return z;
	}
	
	public float a() {
		return a;
	}

	@Override
	public Vector add(Vector v1) {
		
		Vector4Impl v = (Vector4Impl) v1;
		
		float xv = this.x() + v.x();
		float yv = this.y() + v.y();
		float zv = this.z() + v.z();
		float av = this.a() + v.a();
		
		return new Vector4Impl(xv, yv, zv, av);
	}



	@Override
	public Vector sub(Vector v1) {
		
		Vector4Impl v = (Vector4Impl) v1;
		
		float xv = this.x() - v.x();
		float yv = this.y() - v.y();
		float zv = this.z() - v.z();
		float av = this.a() - v.a();
		
		return new Vector4Impl(xv, yv, zv, av);
	}

	@Override
	public Vector mult(float s) {
		
		
		float xv = this.x() * s;
		float yv = this.y() * s;
		float zv = this.z() * s;
		float av = this.a() * s;
		
		return new Vector4Impl(xv, yv, zv, av);
	}

	@Override
	public Vector mult(Vector v1) {
		
		Vector4Impl v = (Vector4Impl) v1;
		
		float xv = this.x() * v.x();
		float yv = this.y() * v.y();
		float zv = this.z() * v.z();
		float av = this.a() * v.a();
		
		return new Vector4Impl(xv, yv, zv, av);
	}

	@Override
	public float length() {
		return (float) Math.sqrt(x()*x()+y()*y()+z()*z()+a()*a());
	}


	@Override
	public Vector normalize() {

		float length = length();
		
		float x = this.x()/length;
		float y = this.y()/length;
		float z = this.z()/length;
		float a = this.a()/length;
				
		return new Vector4Impl(x, y, z, a);
	}

	@Override
	public float dot(Vector v1) {
		Vector4Impl v = (Vector4Impl) v1;
		
		return this.x*v.x()+this.y*v.y()+this.z*v.z()+this.a*v.a();
	}

	@Override
	public Vector cross(Vector v) {
		
		throw new UnsupportedOperationException();
//		return new Vector4Impl(
//				this.y()*v.z()-this.z*v.y(), 
//				this.z()*v.x()-this.x*v.z(), 
//				this.x()*v.y()-this.y*v.x());
	}

	@Override
	public float[] asArray() {
		return new float[] {x,y,z,a};
	}

	@Override
	public FloatBuffer asBuffer() {
		throw new UnsupportedOperationException("NICHT IMPLEMENTIERT");
	}

	@Override
	public void fillBuffer(FloatBuffer buf) {
		throw new UnsupportedOperationException("NICHT IMPLEMENTIERT");
		
	}

	@Override
	public int compareTo(Vector o) {
		return (int) (this.length()-o.length());
	}

	@Override
	public int size() {
		return 4;
	}
	
	@Override
	public String toString() {
		return "Vec:["+x+", "+y+", "+z+", "+a+"]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(Arrays.equals(this.asArray(),((Vector) (obj)).asArray())){
			return true;
		}
		return false;
		
	}
	



}
