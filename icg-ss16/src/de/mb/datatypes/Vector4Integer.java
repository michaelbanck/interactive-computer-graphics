	package de.mb.datatypes;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Vector4Integer {

		public int i0,i1,i2,i3;

		public Vector4Integer(int i0, int i1, int i2, int i3) {
			super();
			this.i0 = i0;
			this.i1 = i1;
			this.i2 = i2;
			this.i3 = i3;
		}
}
