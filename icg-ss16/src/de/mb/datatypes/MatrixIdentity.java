package de.mb.datatypes;

import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

/**
 * ********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
class MatrixIdentity extends MatrixImpl {



	

	protected MatrixIdentity() {
		
				float[] identity =  new float[] 
				{	1,0,0,0,
					0,1,0,0,
					0,0,1,0,
					0,0,0,1	};
		
		matrixData = new MatrixCore(identity);
		matrixInverseData = new MatrixCore(identity);
		
	}




	@Override
	public float[] getValues() {
		return this.matrixData.values;
	}

	@Override
	public void setValues(float[] vals) {
		this.matrixData.setValues(vals);
	}

	@Override
	public float get(int c, int r) {
		return matrixData.get(c, r);
	}

	@Override
	public Matrix mult(Matrix m) {
		MatrixImpl mi = (MatrixImpl) m;
		return new MatrixImpl(mi.matrixData, mi.matrixInverseData);
	}

	/**
	 * i don't know, what this method should do
	 */
	@Override
	public Matrix multSlow(Matrix m) {
		return mult(m);
	}


	@Override
	protected Vector mult(float[] arrayLength4) {
		return new VectorImpl(arrayLength4);
	}


	@Override
	public Matrix transpose() {
		return new MatrixIdentity();
	}

	@Override
	public Matrix invertRigid() {
		return new MatrixIdentity();
	}

	@Override
	public Matrix invertFull() {
		return new MatrixIdentity();

	}


	public Vector getVector(int zeile) {
		return new VectorImpl(get(0, zeile), get(1, zeile), get(2, zeile));
	}

	@Override
	public Matrix getTranslation() {
		return new MatrixIdentity();
	}

	@Override
	public Vector getPosition() {
		return new VectorImpl(0,0,0);
	}





}
