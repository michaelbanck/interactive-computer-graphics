package de.mb.datatypes;

import java.nio.FloatBuffer;
import java.util.Arrays;

import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

/**
 * ********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
class MatrixImpl implements Matrix {

	private static final int columnsCount = 4, rowsCount = 4;

	protected MatrixCore matrixData;
	protected MatrixCore matrixInverseData;

	protected MatrixImpl() {
		matrixData = new MatrixCore();
		matrixInverseData = new MatrixCore();
	}

	protected MatrixImpl(float[] values) {
		super();
		matrixData = new MatrixCore(values);
	}

	protected MatrixImpl(float[] values, float[] inverseValues) {
		super();
		matrixData = new MatrixCore(values);
		matrixInverseData = new MatrixCore(inverseValues);
	}

	protected MatrixImpl(MatrixCore matrix, MatrixCore inverseMatrix) {
		super();
		matrixData = matrix;
		matrixInverseData = inverseMatrix;
	}

	@Override
	public float[] getValues() {
		return this.matrixData.values;
	}

	@Override
	public void setValues(float[] vals) {
		this.matrixData.setValues(vals);
	}

	@Override
	public float get(int c, int r) {
		return matrixData.get(c, r);
	}

	@Override
	public Matrix mult(Matrix m) {

	
		
		MatrixImpl mi = (MatrixImpl) m;
		MatrixImpl newMatrix = new MatrixImpl();

		for (int r = 0; r < rowsCount; r++)
			for (int c = 0; c < columnsCount; c++) {
				float valueXY = 0;
				for (int c2 = 0; c2 < columnsCount; c2++) {
					valueXY += 	mi.matrixData.get(r, c2)*this.matrixData.get(c2, c);		
				}
				newMatrix.matrixData.set(r, c, valueXY);
			}

		if (mi.matrixInverseData != null && this.matrixInverseData != null)
			for (int r = 0; r < rowsCount; r++)
				for (int c = 0; c < columnsCount; c++) {
					float valueXY = 0;
					for (int c2 = 0; c2 < columnsCount; c2++) {
						valueXY += this.matrixInverseData.get(r, c2)*mi.matrixInverseData.get(c2, c);
					}
					newMatrix.matrixInverseData.set(r, c, valueXY);
				}

		return newMatrix;
	}

	/**
	 * i don't know, what this method should do
	 */
	@Override
	public Matrix multSlow(Matrix m) {
		throw new UnsupportedOperationException("NOT IMPLEMENTED");
	}

	@Override // 4. Koor. 1
	public Vector transformPoint(Vector v) {

		float[] array = { v.x(), v.y(), v.z(), 1 };
		return this.mult(array);

	}

	@Override // 4. Koor. 0
	public Vector transformDirection(Vector v) {
		float[] array = { v.x(), v.y(), v.z(), 0 };
		return this.mult(array);
	}

	protected Vector mult(float[] arrayLength4) {

		float val[] = { 0, 0, 0, 0 };

		for (int r = 0; r < 4; r++)
			for (int c = 0; c < 4; c++)
				val[r] += this.matrixData.get(c,r) * arrayLength4[c];

		return new VectorImpl(val);
	}

	@Override
	public Vector transformNormal(Vector v) {
		return transformPoint(v);
	}

	@Override
	public Matrix transpose() {

		MatrixImpl newMatrix = new MatrixImpl();

		for (int r = 0; r < rowsCount; r++)
			for (int c = 0; c < columnsCount; c++) {
				newMatrix.matrixData.set(r, c, this.get(c, r));
			}

		return newMatrix;
	}

	@Override
	public Matrix invertRigid() {
		return new MatrixImpl(matrixInverseData, matrixData);
	}

	@Override
	public Matrix invertFull() {
		return new MatrixImpl(matrixInverseData, matrixData);
//		the real invertFull function is never needed and slow
//		return new MatrixImpl(InverseMatrixCalculator.invert(this.matrixData.values), this.matrixData.values);
	}

	@Override
	public float[] asArray() {
		return matrixData.values;
	}

	@Override
	public FloatBuffer asBuffer() {
		FloatBuffer fB = FloatBuffer.allocate(16);
		fillBuffer(fB);
		return fB;
	}

	@Override
	public void fillBuffer(FloatBuffer buf) {
		for(float f:matrixData.values)
			buf.put(f);
	}

	@Override
	public Matrix getRotation() {
		MatrixImpl m = new MatrixImpl(this.matrixData, this.matrixInverseData);
		m.matrixData.values[12] = 0;
		m.matrixData.values[13] = 0;
		m.matrixData.values[14] = 0;
		
		m.matrixInverseData.values[12] = 0;
		m.matrixInverseData.values[13] = 0;
		m.matrixInverseData.values[14] = 0;
		
		return m;
	}



	public Vector getVector(int zeile) {
		return new VectorImpl(get(0, zeile), get(1, zeile), get(2, zeile));
	}

	@Override
	public Matrix getTranslation() {
		float x =  this.matrixData.values[12];
		float y =  this.matrixData.values[13];
		float z =  this.matrixData.values[14];
		
		
		float[] array = new float[] { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, x, y, z, 1 };
		float[] array2 = new float[] { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, -x, -y, -z, 1 };
		return new MatrixImpl(array, array2);
	}

	@Override
	public Vector getPosition() {
		float x =  this.matrixData.values[12];
		float y =  this.matrixData.values[13];
		float z =  this.matrixData.values[14];

		return new VectorImpl(x, y, z);
	}



	@Override
	public boolean equals(Object obj) {
		return equals((Matrix) obj, 0f);
	}

	@Override
	public boolean equals(Matrix m, float epsilon) {
		if (Arrays.equals(this.asArray(), m.asArray())) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		String s = "[";

		for (int c = 0; c < 4; c++) {
			for (int r = 0; r < 4; r++) {
				s += get(r, c) + ",\t";
			}
			s += "\n";
		}

		return s.substring(0, s.length() - 3) + "]";
	}

}
