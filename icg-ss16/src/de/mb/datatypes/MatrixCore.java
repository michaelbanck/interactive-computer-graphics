package de.mb.datatypes;


/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
class MatrixCore {

	float[] values = new float[16];

	
	MatrixCore() {
	}
	
	public MatrixCore(float[] vals) {
		setValues(vals);
	}
	
	public float get(int c, int r) {
		return values[4 * c + r];
	}
	
	public void set(int c, int r, float value) {
		values[4 * c + r]=value;
	}
	
	public void setValues(float[] vals) {
		this.values = vals;
	}
	

}
