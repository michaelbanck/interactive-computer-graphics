package de.mb.datatypes;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Vector3Integer {

	// indices  faces v, vt, vn
	public int i0, i1, i2;

	protected Vector3Integer() {
		super();
	}

	public Vector3Integer(int i0, int i1, int i2) {
		this();

		this.i0 = i0;
		this.i1 = i1;
		this.i2 = i2;

	}

	public int index0() {
		return i0;
	}

	public int index1() {
		return i1;
	}

	public int index2() {
		return i2;
	}

}
