	package de.mb.datatypes;

import java.nio.FloatBuffer;
import java.util.Arrays;

import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class VectorImpl implements Vector{

	public float x,y,z;
	
	public VectorImpl(){
		super();
	}
	
	public VectorImpl(float x, float y, float z) {
		this();

		this.x = x;
		this.y = y;
		this.z = z;

	}
	
	public VectorImpl(float[] values) {
		this(values[0],values[1],values[2]);
	}
	
	public void setX(float x){
		this.x = x;
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public void setZ(float z){
		this.z = z;
	}

	@Override
	public float x() {
		return x;
	}

	@Override
	public float y() {
		return y;
	}

	@Override
	public float z() {
		return z;
	}

	@Override
	public Vector add(Vector v) {
		
		
		float xv = this.x() + v.x();
		float yv = this.y() + v.y();
		float zv = this.z() + v.z();
		
		return new VectorImpl(xv, yv, zv);
	}



	@Override
	public Vector sub(Vector v) {
		
		float xv = this.x() - v.x();
		float yv = this.y() - v.y();
		float zv = this.z() - v.z();
		
		return new VectorImpl(xv, yv, zv);
	}

	@Override
	public Vector mult(float s) {
		
		float xv = this.x() * s;
		float yv = this.y() * s;
		float zv = this.z() * s;
		
		return new VectorImpl(xv, yv, zv);
	}

	@Override
	public Vector mult(Vector v) {
		
		float xv = this.x() * v.x();
		float yv = this.y() * v.y();
		float zv = this.z() * v.z();
		
		return new VectorImpl(xv, yv, zv);
	}

	@Override
	public float length() {
		return (float) Math.sqrt(x()*x()+y()*y()+z()*z());
	}


	@Override
	public Vector normalize() {

		float length = length();
		
		float x = this.x()/length;
		float y = this.y()/length;
		float z = this.z()/length;
				
		return new VectorImpl(x, y, z);
	}

	@Override
	public float dot(Vector v) {
		return this.x*v.x()+this.y*v.y()+this.z*v.z();
	}

	@Override
	public Vector cross(Vector v) {
		return new VectorImpl(
				this.y()*v.z()-this.z*v.y(), 
				this.z()*v.x()-this.x*v.z(), 
				this.x()*v.y()-this.y*v.x());
	}

	@Override
	public float[] asArray() {
		return new float[] {x,y,z};
	}

	@Override
	public FloatBuffer asBuffer() {
		FloatBuffer fB = FloatBuffer.allocate(3);
		fillBuffer(fB);
		
		return fB;
	}

	@Override
	public void fillBuffer(FloatBuffer fB) {
		fB.put(x);
		fB.put(y);
		fB.put(z);
	}


	@Override
	public int compareTo(Vector v) {
		if(v.x() != this.x)
			return -1;
		if(v.y() != this.y)
			return -1;
		if(v.z() != this.z)
			return -1;
		
		return 1;

	}

	@Override
	public int size() {
		return 3;
	}
	
	@Override
	public String toString() {
		return "Vec:["+x+", "+y+", "+z+"]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(Arrays.equals(this.asArray(),((Vector) (obj)).asArray())){
			return true;
		}
		return false;
		
	}
	



}
