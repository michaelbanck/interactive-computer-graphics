package de.mb.sceneparts;

import de.mb.datatypes.MyMathFactory;
import de.mb.models.Model;
import de.mb.models.OBJLoader;
import de.mb.models.contants.Models;
import de.mb.models.contants.Textures;
import de.mb.scenegraph.actionnodes.BallAction;
import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class BallGame {

	private float areaSize = 30;
	private Node root;
	private Model mCube = OBJLoader.loadModel(Models.CUBE_CENTERED);
	private Model mNaboo = OBJLoader.loadModel(Models.NABOO);
	private NodeBuilder builder = NodeBuilder.getInstance();

	Factory myFactory = MyMathFactory.vecmath;

	public BallGame(Node root) {
		this.root = root;
		initSzene();

	}

	private void initSzene() {

		createWalls();
		createLights();
		

		Node gate1 = builder.createGroup(root, myFactory.translationMatrix(0, 0, 15));
		Vector pos1 = createGate(Textures.RED, gate1, 1);

		Node gate2 = builder.createGroup(root, myFactory.translationMatrix(15, 0, -15));
		Vector pos2 = createGate(Textures.BLUE, gate2, 2);

		Node gate3 = builder.createGroup(root, myFactory.translationMatrix(-15, 0, -15));
		Vector pos3 = createGate(Textures.GREEN, gate3, 3);
		
		BallAction bA = createBall();
		bA.addGate(pos1, Textures.RED);
		bA.addGate(pos2, Textures.BLUE);
		bA.addGate(pos3, Textures.GREEN);
	}

	private void createLights() {
		Matrix mID = myFactory.identityMatrix();

		Matrix tran = myFactory.translationMatrix(0, 8, -15).mult(myFactory.scaleMatrix(0.3f, 0.3f, 0.3f));
		GeoNode ballLight = builder.createGeoNode(root, tran, mCube).setLight(true).setTexturePath(Textures.WHITE);
		builder.createLightNode(ballLight, mID);


		tran = myFactory.translationMatrix(0, 8, 15).mult(myFactory.scaleMatrix(0.3f, 0.3f, 0.3f));
		ballLight = builder.createGeoNode(root, tran, mCube).setLight(true).setTexturePath(Textures.WHITE);
		builder.createLightNode(ballLight, mID);


	}

	private GeoNode createWalls() {
		

		Matrix scaleGround = myFactory.scaleMatrix(areaSize, 1, areaSize).mult(myFactory.translationMatrix(0,-2,0));
		
		builder.createGeoNode(root, scaleGround, mCube).setTexturePath(Textures.RIFFEL);
		
		Matrix scaleCube = myFactory.translationMatrix(0, 0, -areaSize).mult(myFactory.scaleMatrix(areaSize+1, 1f, 1f));
		GeoNode cubeNode = builder.createGeoNode(root, scaleCube, mCube).setTexturePath(Textures.GRAY);

		scaleCube = myFactory.translationMatrix(0, 0, areaSize).mult(myFactory.scaleMatrix(areaSize+1, 1f, 1f));
		cubeNode = builder.createGeoNode(root, scaleCube, mCube).setTexturePath(Textures.GRAY);

		scaleCube = myFactory.translationMatrix(areaSize, 0, 0).mult(myFactory.scaleMatrix(1f, 1f, areaSize+1));
		cubeNode = builder.createGeoNode(root, scaleCube, mCube).setTexturePath(Textures.GRAY);

		scaleCube = myFactory.translationMatrix(-areaSize, 0, 0).mult(myFactory.scaleMatrix(1f, 1f, areaSize+1));
		cubeNode = builder.createGeoNode(root, scaleCube, mCube).setTexturePath(Textures.GRAY);

		return cubeNode;
	}

	private BallAction createBall() {
		float nabooScale = 0.002f;
		Node ballActionNode = builder.createGroup(root, myFactory.translationMatrix(0, 0.1f, 0));
		
		GeoNode ball = builder.createGeoNode(ballActionNode,
			myFactory.scaleMatrix(nabooScale * 4, nabooScale * 4, nabooScale * 4),
				mNaboo).setTexturePath(Textures.RED);

		BallAction bA = new BallAction(areaSize, ball);
		ball.addActionNode(bA);
		
		
		return bA;
	}

	private Vector createGate(String texture, Node rootNode, int nr) {

		
		
		Matrix scaleCube = myFactory.translationMatrix(-3, 0, 0).mult(myFactory.scaleMatrix(1, 3f, 1f));
		builder.createGeoNode(rootNode, scaleCube, mCube).setTexturePath(texture);

		scaleCube = myFactory.translationMatrix(3, 0, 0).mult(myFactory.scaleMatrix(1, 3f, 1f));
		builder.createGeoNode(rootNode, scaleCube,  mCube).setTexturePath(texture);

		scaleCube = myFactory.translationMatrix(0, 4, 0).mult(myFactory.scaleMatrix(4.5f, 1f, 1f));
		builder.createGeoNode(rootNode, scaleCube,  mCube).setTexturePath(texture);

		Matrix m = rootNode.matrix.mult(scaleCube);
		Vector pos = m.getPosition();
		return pos;
	}

}
