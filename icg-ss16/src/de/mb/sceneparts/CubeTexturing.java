package de.mb.sceneparts;

import de.mb.datatypes.MyMathFactory;
import de.mb.models.Model;
import de.mb.models.OBJLoader;
import de.mb.models.contants.Models;
import de.mb.models.contants.Normals;
import de.mb.models.contants.Specular;
import de.mb.models.contants.Textures;
import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;
import ogl.vecmathimp.VectorImp;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class CubeTexturing {

	private Node root;
	private Factory myFactory = MyMathFactory.vecmath;
	private Model mCube = OBJLoader.loadModel(Models.CUBE_CENTERED);
	private Model mNaboo = OBJLoader.loadModel(Models.NABOO);
	private NodeBuilder builder = NodeBuilder.getInstance();
	private final float nabooToLight = 0.03f;
	private final Vector scaleV = new VectorImp(nabooToLight, nabooToLight, nabooToLight);

	public CubeTexturing(Node root) {
		this.root = root;
		initSzene();
	}

	private void initSzene() {
	
		this.createNode(-3, 1, Textures.BRICKS);
		this.createNode(0, 1, Textures.BRICKS).setNormalPath(Normals.BRICKS);
		this.createNode(3, 1, Textures.BRICKS).setSpecularPath(Specular.BRICKS).setNormalPath(Normals.BRICKS);
		 

	}

	private GeoNode createNode(int positionZ, int nr, String texturPath) {

		Matrix scaleCube = myFactory.scaleMatrix(3.5f, 3.5f, 3.5f)
				.mult(MyMathFactory.vecmath.translationMatrix(0, 0, positionZ));

		GeoNode cube = builder.createGeoNode(root, scaleCube, mCube).setTexturePath(texturPath);

		// axis of the lights, with rotator
		Node axis = builder.createGroup(cube, MyMathFactory.vecmath.translationMatrix(5, 0, 0));

		Matrix scaleLight = myFactory.scaleMatrix(scaleV.mult(0.01f));

		builder.createLightNode(axis, (scaleLight));
		builder.createGeoNode(axis, (scaleLight), mNaboo).setTexturePath(Textures.WHITE).setLight(true);

		return cube;

	}

}
