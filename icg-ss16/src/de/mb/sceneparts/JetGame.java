package de.mb.sceneparts;

import de.mb.datatypes.MyMathFactory;
import de.mb.models.Model;
import de.mb.models.OBJLoader;
import de.mb.models.contants.Models;
import de.mb.models.contants.Textures;
import de.mb.scenegraph.actionnodes.FlyJetAction;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;
import ogl.vecmath.Factory;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class JetGame {


	Model modelCube = OBJLoader.loadModel(Models.F16);
	private Factory myFactory = MyMathFactory.vecmath;
	private NodeBuilder builder = NodeBuilder.getInstance();
	
	public JetGame(Node root) {
		
		Node jet = builder.createGeoNode(root, myFactory.identityMatrix(), modelCube).setTexturePath(Textures.F16s).addActionNode(new FlyJetAction());
		
		Node egoNode = builder.createGroup(jet, myFactory.rotationMatrix(0,1,0, -90).mult(myFactory.translationMatrix(0, 0.1f, 1.9f)));
		builder.createCamNode(egoNode);
		
		Node farNode = builder.createGroup(jet, myFactory.rotationMatrix(0,1,0, -90).mult(myFactory.translationMatrix(0, 1, 4)));
		builder.createCamNode(farNode);
		
		}

	

}
