package de.mb.sceneparts;

import de.mb.datatypes.MyMathFactory;
import de.mb.models.Model;
import de.mb.models.OBJLoader;
import de.mb.models.contants.Models;
import de.mb.models.contants.Normals;
import de.mb.models.contants.Specular;
import de.mb.models.contants.Textures;
import de.mb.scenegraph.actionnodes.RotorAction;
import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;
import ogl.vecmathimp.VectorImp;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Cube3Lights {


	private Node root;
	private Model mCube = OBJLoader.loadModel(Models.CUBE_CENTERED);
	private Model mNaboo = OBJLoader.loadModel(Models.NABOO);
	private NodeBuilder builder = NodeBuilder.getInstance();

	private Factory myFactory = MyMathFactory.vecmath;

	public Cube3Lights(Node root) {
		this.root = root;


		// create a red cube with 3 lights
		createNode(0, 1, Textures.RED);

		// create a textured cube with 3 lights above the red
		createNode(10, 2, Textures.BRICKS).setNormalPath(Normals.BRICKS).setSpecularPath(Specular.BRICKS);
		
		
	}

	private GeoNode createNode(float transY, int nr, String texturePath) {

		
		// scale the cube model
		Matrix scaleCube = myFactory.translationMatrix(0, transY, 0).mult(myFactory.scaleMatrix(3.5f, 3.5f, 3.5f));

		// create cube in the middle of 3 lights
		GeoNode cubeNode = builder.createGeoNode(root, scaleCube, mCube).setTexturePath(texturePath);

		// scaling factors
		float nabooToLight = 0.03f;
		Vector scaleV = new VectorImp(nabooToLight, nabooToLight, nabooToLight);

		// axis of the lights, with rotator
		Node axis = builder.createGroup(cubeNode, myFactory.scaleMatrix(scaleV.mult(25))).addActionNode(new RotorAction(20, new VectorImp(0, 1, 0)));
				

		// create 3 lights and geoNodes at the same postion
		Matrix scaleLight = myFactory.scaleMatrix(scaleV.mult(0.01f));

		GeoNode ballLight = builder.createGeoNode(axis, myFactory.translationMatrix(0, 0, 4).mult(scaleLight), mNaboo).setTexturePath(Textures.WHITE).setLight(true);

		builder.createLightNode(ballLight, myFactory.identityMatrix());

//		ballLight = builder.createGeoNode(axis, myFactory.translationMatrix(0, 0, -4).mult(scaleLight), mNaboo,
//				nameNode + "LIGHT_CUBE_MODEL2", Textures.WHITE, null).setLight(true);
//
//		builder.createLightNode(ballLight, myFactory.identityMatrix(), nameNode + "BALL_LIGHT2");

		ballLight = builder.createGeoNode(axis, myFactory.translationMatrix(4, 0, 0).mult(scaleLight), mNaboo).setTexturePath(Textures.WHITE).setLight(true);

		builder.createLightNode(ballLight, myFactory.identityMatrix());

		return cubeNode;
	}

}
