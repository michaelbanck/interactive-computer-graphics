package de.mb.sceneparts;

import de.mb.datatypes.MyMathFactory;
import de.mb.effects.Sounds;
import de.mb.models.Model;
import de.mb.models.OBJLoader;
import de.mb.models.contants.Models;
import de.mb.models.contants.Textures;
import de.mb.scenegraph.actionnodes.SimpleJumperAction;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;
import ogl.vecmath.Factory;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class StoneJumper {


	private Model modelCube = OBJLoader.loadModel(Models.CUBE_CENTERED);
	private Factory myFactory = MyMathFactory.vecmath;
	private NodeBuilder builder = NodeBuilder.getInstance();

	public StoneJumper(Node root) {
		builder.createGeoNode(root, myFactory.identityMatrix(), modelCube).setTexturePath(Textures.STONE)
				.addActionNode(new SimpleJumperAction(0.4f, SimpleJumperAction.Y_AXSIS, 3, 0).setSoundPath(Sounds.STONE));
	}

}
