package de.mb.sceneparts;

import de.mb.datatypes.MyMathFactory;
import de.mb.datatypes.VectorImpl;
import de.mb.models.Model;
import de.mb.models.OBJLoader;
import de.mb.models.contants.Models;
import de.mb.models.contants.Textures;
import de.mb.scenegraph.actionnodes.ActionNode;
import de.mb.scenegraph.actionnodes.CameraAction;
import de.mb.scenegraph.actionnodes.RotorAction;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmathimp.VectorImp;

public class SolarSystem {

	private static Factory math = MyMathFactory.vecmath;
	private static boolean isCreated = false;
	
	public static void getSolarSystem(Node startNode){
		
		
		if(isCreated)
			return;
		
		
		
		Matrix mDefaultScale = math.scaleMatrix(0.00001f, 0.00001f, 0.00001f);

		
		
		//planetSize
		Matrix mMecurySize = math.scaleMatrix(4.8f, 4.8f, 4.8f).mult(mDefaultScale);
		Matrix mVenusSize = math.scaleMatrix(12.1f, 12.1f, 12.1f).mult(mDefaultScale);
		Matrix mEarthSize = math.scaleMatrix(12.7f, 12.7f, 12.7f).mult(mDefaultScale);
		Matrix mMarsSize = math.scaleMatrix(6.7f, 6.7f, 6.7f).mult(mDefaultScale);
		Matrix mJupiterSize = math.scaleMatrix(138, 138, 138).mult(mDefaultScale);
		Matrix mSaturnSize = math.scaleMatrix(114, 114, 114).mult(mDefaultScale);
		Matrix mUranusSize =  math.scaleMatrix(50, 50, 50).mult(mDefaultScale);
		Matrix mNeptuneSize = math.scaleMatrix(49, 49, 49).mult(mDefaultScale);
		Matrix mSunSize = math.scaleMatrix(139.0f*2, 139.0f*2, 139.0f*2).mult(mDefaultScale);
		
		
		//axis for different rotation speed of the planets
		Matrix mMecuryR = math.translationMatrix(0,0,0);
		Matrix mVenusR = math.translationMatrix(0,0,0);
		Matrix mEarthR = math.translationMatrix(0,0,0);
		Matrix mMarsR = math.translationMatrix(0,0,0);
		Matrix mJupiterR = math.translationMatrix(0,0,0);
		Matrix mSaturnR = math.translationMatrix(0,0,0);
		Matrix mUranusR = math.translationMatrix(0,0,0);
		Matrix mNeptuneR = math.translationMatrix(0,0,0);
		Matrix mSunR = math.translationMatrix(0,0,0);

		
		
		//distance to sun
		float scaleTran = 0.5f;
		float scaleTran2 = 0.1f;
		Matrix mMecuryTran = math.translationMatrix(0,0,0.58f*scaleTran);
		Matrix mVenusTran = math.translationMatrix(0,0,1.08f*scaleTran);
		Matrix mEarthTran = math.translationMatrix(0,0,1.5f*scaleTran);
		Matrix mMarsTran = math.translationMatrix(0,0,2.28f*scaleTran);
		Matrix mJupiterTran = math.translationMatrix(0,0,7.78f*scaleTran);
		Matrix mSaturnTran = math.translationMatrix(0,0,14.33f*scaleTran2);
		Matrix mUranusTran = math.translationMatrix(0,0,28.72f*scaleTran2);
		Matrix mNeptuneTran = math.translationMatrix(0,0,44.95f*scaleTran2);
		Matrix mSunTran = math.translationMatrix(0,0,0);

	
		Matrix rot = math.rotationMatrix(new VectorImp(1, 0, 0), 90);
		
		
		Matrix mMecurySizeTran = mMecuryTran.mult(rot.mult(mMecurySize));
		Matrix mVenusSizeTran =  mVenusTran.mult(rot.mult(mVenusSize));
		Matrix mEarthSizeTran = mEarthTran.mult(rot.mult(mEarthSize));
		Matrix mMarsSizeTran =  mMarsTran.mult(rot.mult(mMarsSize));
		Matrix mJupiterSizeTran =  mJupiterTran.mult(rot.mult(mJupiterSize));
		Matrix mSaturnSizeTran =  mSaturnTran.mult(rot.mult(mSaturnSize));
		Matrix mUranusSizeTran =   mUranusTran.mult(rot.mult(mUranusSize));
		Matrix mNeptuneSizeTran =  mNeptuneTran.mult(rot.mult(mNeptuneSize));
		Matrix mSunSizeTran =  mSunTran.mult(rot.mult(mSunSize));
//		Matrix mMoonSizeTran =  mMoonTran.mult(rot.mult(mMoonSize));
		
		
		
		//position and size
		float speed = 0.5f;
		
		//rotation speed about the sun
		float speedMecuryRoation = 172*speed;
		float speedVenusRoation = 126*speed;
		float speedEarthRoation = 107*speed;
		float speedMarsRoation =86*speed;
		float speedJupiterRoation = 47*speed;
		float speedSaturnRoation = 34*speed;
		float speedUranusRoation =24*speed;
		float speedNeptuneRoation = 19*speed;
		float speedMoonRoation = 700*speed;
		

		
		//actionRotators 
		ActionNode anMercury = new RotorAction(speedMecuryRoation, new VectorImpl(0, 1, 0));
		ActionNode anVenus = new RotorAction(speedVenusRoation, new VectorImpl(0, 1, 0));
		ActionNode anEarth = new RotorAction(speedEarthRoation, new VectorImpl(0, 1, 0));
		ActionNode anMars = new RotorAction(speedMarsRoation, new VectorImpl(0, 1, 0));
		ActionNode anJupiter = new RotorAction(speedJupiterRoation, new VectorImpl(0, 1, 0));
		ActionNode anSaturn = new RotorAction(speedSaturnRoation, new VectorImpl(0, 1, 0));
		ActionNode anUranus = new RotorAction(speedUranusRoation, new VectorImpl(0, 1, 0));
		ActionNode anNeptune = new RotorAction(speedNeptuneRoation, new VectorImpl(0, 1, 0));
		ActionNode anMoon = new RotorAction(speedMoonRoation, new VectorImpl(0, 1, 0));
		
		
		
		
		NodeBuilder builder = NodeBuilder.getInstance();
		Node sunAxis = builder.createGroup(startNode, mSunR);
		
		//Axis about the sun 
		Node mercuryAxis =builder.createGroup(sunAxis, mMecuryR).addActionNode(anMercury);
		Node venusAxis =builder.createGroup(sunAxis, mVenusR).addActionNode(anVenus);
		Node EarthAxis =builder.createGroup(sunAxis, mEarthR).addActionNode(anEarth);
		Node marsAxis =builder.createGroup(sunAxis, mMarsR).addActionNode(anMars);
		Node jupiterAxis =builder.createGroup(sunAxis, mJupiterR).addActionNode(anJupiter);
		Node saturnAxis =builder.createGroup(sunAxis, mSaturnR).addActionNode(anSaturn);
		Node uranusAxis =builder.createGroup(sunAxis, mUranusR).addActionNode(anUranus);
		Node neptuneAxis =builder.createGroup(sunAxis, mNeptuneR).addActionNode(anNeptune);
		Node earthMoonAxis = builder.createGroup(EarthAxis, mEarthTran).addActionNode(anMoon);
		
		Node moonAxis =builder.createGroup(earthMoonAxis, math.translationMatrix(0,0,0.05f)).addActionNode(anMoon);
	
	
		
		
		
		
		float speedSR = -2.5f;
		
		//rotation speed about the  own axis
		float speedMecuryRoationSR = 172*speedSR;
		float speedVenusRoationSR = 126*speedSR;
		float speedEarthRoationSR = 107*speedSR;
		float speedMarsRoationSR =86*speedSR;
		float speedJupiterRoationSR = 47*speedSR;
		float speedSaturnRoationSR = 34*speedSR;
		float speedUranusRoationSR =24*speedSR;
		float speedNeptuneRoationSR = 19*speedSR;
		float speedMoonRoationSR = 100*speedSR;
		
		
		//planets rotation about the own axis
		ActionNode anMercurySR = new RotorAction(speedMecuryRoationSR, new VectorImpl(0, 0, 1));
		ActionNode anVenusSR = new RotorAction(speedVenusRoationSR, new VectorImpl(0, 0, 1));
		ActionNode anEarthSR = new RotorAction(speedEarthRoationSR, new VectorImpl(0, 0, 1));
		ActionNode anMarsSR = new RotorAction(speedMarsRoationSR, new VectorImpl(0, 0, 1));
		ActionNode anJupiterSR = new RotorAction(speedJupiterRoationSR, new VectorImpl(0, 0, 1));
		ActionNode anSaturnSR = new RotorAction(speedSaturnRoationSR, new VectorImpl(0, 0, 1));
		ActionNode anUranusSR = new RotorAction(speedUranusRoationSR, new VectorImpl(0, 0, 1));
		ActionNode anNeptuneSR = new RotorAction(speedNeptuneRoationSR, new VectorImpl(0, 0, 1));
		ActionNode anSunSR = new RotorAction(speedNeptuneRoationSR, new VectorImpl(1, 1, 1));
		ActionNode anMoonSR = new RotorAction(speedMoonRoationSR, new VectorImpl(0, 0, 1));
		
		
		
		Model model = OBJLoader.loadModel(Models.NABOO);
		
		
		builder.createGeoNode(mercuryAxis, mMecurySizeTran, model).setTexturePath(Textures.MERCURY).addActionNode(anMercurySR);
		builder.createGeoNode(venusAxis, mVenusSizeTran, model).setTexturePath(Textures.VENUS).addActionNode(anVenusSR);
		builder.createGeoNode(EarthAxis, mEarthSizeTran, model).setTexturePath(Textures.EARTH).addActionNode(anEarthSR); 
		builder.createGeoNode(marsAxis, mMarsSizeTran, model).setTexturePath(Textures.MARS).addActionNode(anMarsSR); 
		builder.createGeoNode(jupiterAxis, mJupiterSizeTran, model).setTexturePath(Textures.JUPITER).addActionNode(anJupiterSR); 
		builder.createGeoNode(saturnAxis, mSaturnSizeTran, model).setTexturePath(Textures.SATURN).addActionNode(anSaturnSR); 
		builder.createGeoNode(uranusAxis, mUranusSizeTran, model).setTexturePath(Textures.URANUS).addActionNode(anUranusSR); 
		builder.createGeoNode(neptuneAxis, mNeptuneSizeTran, model).setTexturePath(Textures.NEPTUNE).addActionNode(anNeptuneSR); 
		
		builder.createGeoNode(moonAxis, math.scaleMatrix(0.00002f,0.00002f,0.00002f), model).setTexturePath(Textures.MOON).addActionNode(anMoonSR);
		
		Node geoSun = builder.createGeoNode(sunAxis, mSunSizeTran, model).setLight(true).setTexturePath(Textures.SUN).addActionNode(anSunSR);																
		builder.createLightNode(geoSun, math.translationMatrix(0,0, 0));
	
		Node node = builder.createGroup(startNode, math.translationMatrix(0,0,1));
		
		builder.createCamNode(node).addActionNode(new CameraAction());

		isCreated = true;
		
	}
	
	
}
