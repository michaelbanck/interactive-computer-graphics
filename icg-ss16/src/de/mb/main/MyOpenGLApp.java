package de.mb.main;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glBindAttribLocation;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUseProgram;

import java.util.ArrayList;
import java.util.List;

import de.mb.datatypes.MyMathFactory;
import de.mb.datatypes.VectorImpl;
import de.mb.helper.Manipulators;
import de.mb.scenegraph.nodes.Crosshair;
import de.mb.scenegraph.nodes.GeoNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.traverser.Traverser;
import de.mb.scenegraph.traverser.TraverserActionNodes;
import de.mb.scenegraph.traverser.TraverserBuilder;
import de.mb.scenegraph.traverser.TraverserLightCounter;
import de.mb.shader.FragmentShader;
import de.mb.shader.LightArrayUniform;
import de.mb.shader.VertexShader;
import ogl.app.App;
import ogl.app.Input;
import ogl.app.MatrixUniform;
import ogl.app.Util;
import ogl.app.VectorUniform;
import ogl.app.VertexArrayObject;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

public class MyOpenGLApp implements App {

	public static long window;
	private Factory myFactory = MyMathFactory.vecmath;
	// shader pass variables
	private int textint, isLight, normalint, specularint, lightCountShader, lightCount, shaderBrightness, program,
			hasNormals, hasSpecular;
	private float brightness = 1.0f;
	// holds the specular, diffuse and ambient coefficient
	private VectorImpl lightCoefficient = new VectorImpl(1, 1, 1);
	// matrices for vertex shader
	private MatrixUniform normalMatrixUniform, modelViewMatrixUniform, projectionMatrixUniform;
	// shader variable to pass the light coefficients
	private VectorUniform lightParts;
	// array of dif. light sources
	private LightArrayUniform lightArrayUniform;

	private Node root;
	private Matrix viewMatrix = MyMathFactory.vecmath.identityMatrix();
	private final float farPlane = 100f;
	private final float fovy = 60f;
	private List<Vector> listLight = new ArrayList<Vector>();
	private Crosshair crosshair = new Crosshair();

	public MyOpenGLApp(Node root) {
		super();
		this.root = root;
	}

	private void initTextures() {

		System.setProperty(" java .awt . headless ", " true ");
		root.accept(new TraverserBuilder());

	}

	private void initShader() {

		// Create and compile the vertex shader.
		int vs = new VertexShader().getShaderID();

		// Create and compile the fragment shader.
		int fs = new FragmentShader().getShaderID();

		program = glCreateProgram();
		glAttachShader(program, vs);
		glAttachShader(program, fs);

		// Bind the vertex attribute data locations for this shader program. The
		// shader expects to get vertex and color data from the mesh. This needs
		// to
		// be done *before* linking the program.
		glBindAttribLocation(program, VertexArrayObject.vertexAttribIdx, "vertex");
		glBindAttribLocation(program, VertexArrayObject.colorAttribIdx, "color");
		glBindAttribLocation(program, VertexArrayObject.normalAttribIdx, "normal");
		glBindAttribLocation(program, VertexArrayObject.textureAttribIdx, "texCoord");

		// Link the shader program.
		glLinkProgram(program);
		Util.checkLinkage(program);
		glUseProgram(program);
	}

	@Override
	public void init() {
		initShader();
		initTextures();

		// Set background color to black.
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glEnable(GL_DEPTH_TEST);

		// Bind the matrix uniforms to locations on this shader program. This
		// needs
		// to be done *after* linking the program.

		TraverserLightCounter t = new TraverserLightCounter();
		root.accept(t);

		lightCount = t.getCount();

		normalMatrixUniform = new MatrixUniform(program, "normalMatrix");
		modelViewMatrixUniform = new MatrixUniform(program, "modelViewMatrix");
		projectionMatrixUniform = new MatrixUniform(program, "projectionMatrix");
		lightParts = new VectorUniform(program, "lightParts");
		lightArrayUniform = new LightArrayUniform(program, "lightArray", lightCount);
		textint = glGetUniformLocation(program, "tex");
		isLight = glGetUniformLocation(program, "isLight");
		normalint = glGetUniformLocation(program, "normalText");
		hasNormals = glGetUniformLocation(program, "hasNormals");
		hasSpecular = glGetUniformLocation(program, "hasSpecular");
		specularint = glGetUniformLocation(program, "specularText");
		lightCountShader = glGetUniformLocation(program, "lightCount");
		shaderBrightness = glGetUniformLocation(program, "brightness");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cg2.cube.App#simulate(float, cg2.cube.Input)
	 */
	@Override
	public void simulate(float elapsed, Input input) {

		lightCoefficient = Manipulators.changeCoefficient(input, lightCoefficient);
		brightness = Manipulators.changeCoefficient(input, brightness);
		Manipulators.nextCam(input);
		Manipulators.saveScene(input, root);

		// simulate each ActionNode at the scene-graph
		root.accept(new TraverserActionNodes(elapsed, input));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cg2.cube.App#display(int, int, javax.media.opengl.GL2ES2)
	 */
	@Override
	public void display(int width, int height) {

		// Adjust the the viewport to the actual window size. This makes the
		// rendered image fill the entire window.
		glViewport(0, 0, width, height);

		// Clear all buffers.
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Assemble the transformation matrix that will be applied to all
		// vertices in the vertex shader.
		float aspect = (float) width / (float) height;

		// The perspective projection. Camera space to NDC.
		Matrix projectionMatrix = myFactory.perspectiveMatrix(fovy, aspect, 0.1f, farPlane);
		
		
		glUniform1f(shaderBrightness, brightness);
		glUniform1i(lightCountShader, lightCount);
		lightParts.set(lightCoefficient);
		projectionMatrixUniform.set(projectionMatrix);
		
	
		// calculate each Modelmatrix
		root.accept(new Traverser().setMyOpenGLApp(this));
		bindDrawUnbindNode(crosshair, viewMatrix.invertFull());
		lightArrayUniform.set(listLight);
		listLight = new ArrayList<Vector>();

	}

	public void addLight(Matrix modellight) {
		Matrix mvMatrix = viewMatrix.mult(modellight);
		Vector lightPosition = mvMatrix.transformPoint(new VectorImpl(0, 0, 0));

		listLight.add(lightPosition);
	}


	public void bindDrawUnbindNode(GeoNode node, Matrix model) {

		if (node.isLight()) {
			glUniform1i(isLight, 1);
		} else {
			glUniform1i(isLight, 0);
		}

		glUniform1i(textint, node.getTextureNumber());
		glUniform1i(normalint, node.getNormalNumber());
		glUniform1i(specularint, node.getSpecularNumber());
		glUniform1i(hasNormals, node.getNormalNumber());
		glUniform1i(hasSpecular, node.getSpecularNumber());
		

		Matrix modelViewMatrix = viewMatrix.mult(model);
		Matrix normalMatrix = modelViewMatrix.invertFull().transpose();


		modelViewMatrixUniform.set(modelViewMatrix);
		normalMatrixUniform.set(normalMatrix);
	

		node.getVertexArrayObject().bind();
		node.getVertexArrayObject().draw();
		node.getVertexArrayObject().unbind();

	}
	
	@Override
	public void cleanUp() {
		// listen Elemente werden überschrieben, daher kein cleanUp
	}

	public void setCam(Matrix viewMatrix) {
		this.viewMatrix = viewMatrix;
	}

	public void setWindow(long win) {
		window = win;
	}

	public long getWindow() {
		return window;
	}

	
}
