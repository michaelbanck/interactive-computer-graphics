package de.mb.json;


import java.awt.MouseInfo;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.mb.scenegraph.actionnodes.ActionNode;
import de.mb.scenegraph.nodes.GroupNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.traverser.TraverserLoader;
import ogl.vecmath.Color;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;
import ogl.vecmath.Vector;

public class SceneLoader {

	
	public static boolean loadDialog(){

		String[] options = {"Ja", "Nein"};
		int i = JOptionPane.showOptionDialog(null, "Szene aus Filesystem laden?", "Laden Ja/Nein", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		if(i == 0)
			return true;
		
		return false;
	}
	
	public static void write(Node rootNode) {

		File file = getFileChooser(".json", ".json", null);

		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Node.class, new InterfaceAdapter<Node>());
		gsonBuilder.registerTypeAdapter(Matrix.class, new InterfaceAdapter<Matrix>());
		gsonBuilder.registerTypeAdapter(Vector.class, new InterfaceAdapter<Vector>());
		gsonBuilder.registerTypeAdapter(Color.class, new InterfaceAdapter<Color>());
		gsonBuilder.registerTypeAdapter(ActionNode.class, new InterfaceAdapter<ActionNode>());
		gsonBuilder.registerTypeAdapter(Factory.class, new InterfaceAdapter<Factory>());
		
		Gson gson = gsonBuilder.create();
		String jsonString = gson.toJson(rootNode);

		try {
			FileOutputStream fOut = new FileOutputStream(file);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			myOutWriter.append(jsonString);
			myOutWriter.close();
			fOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public static Node read() {

		File file = getFileChooser(".json", "Lade Szene aus .json file", null);
		

		try {
			 
		        FileInputStream fIn = new FileInputStream(file);
		        BufferedReader myReader = new BufferedReader(new InputStreamReader(fIn));
		        String aDataRow = "";
		        String aBuffer = ""; 
		        while ((aDataRow = myReader.readLine()) != null) 
		        {
		            aBuffer += aDataRow ;
		        }
		        myReader.close();

		       
				GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.registerTypeAdapter(Node.class, new InterfaceAdapter<Node>());
				gsonBuilder.registerTypeAdapter(Matrix.class, new InterfaceAdapter<Matrix>());
				gsonBuilder.registerTypeAdapter(Vector.class, new InterfaceAdapter<Vector>());
				gsonBuilder.registerTypeAdapter(Color.class, new InterfaceAdapter<Color>());
				gsonBuilder.registerTypeAdapter(ActionNode.class, new InterfaceAdapter<ActionNode>());
				gsonBuilder.registerTypeAdapter(Factory.class, new InterfaceAdapter<Factory>());
				
				Gson gson = gsonBuilder.create();
		      
				
				Node node = gson.fromJson(aBuffer, GroupNode.class);
				TraverserLoader t = new TraverserLoader(null);
				t.visitNode(node);
				
				
		        return node;
		        
		

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	
	public static File getFileChooser(final String extension, final String description, String stdpath) {

		FileFilter ff = new FileFilter() {

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public boolean accept(File f) {
				String name = f.getName();

				if (name != null && (name.endsWith(extension) || f.isDirectory())) {
					return true;
				}
				return false;
			}
		};

		File workingDirectory;

		if (stdpath == null)
			workingDirectory = new File(java.lang.System.getProperty("user.dir"));
		else
			workingDirectory = new File(stdpath);

		Point p = MouseInfo.getPointerInfo().getLocation();

		JFileChooser chooser = new JFileChooser();
		chooser.setLocation(p);
		chooser.setCurrentDirectory(workingDirectory);
		chooser.setFileFilter(ff);
		chooser.addChoosableFileFilter(ff);
		chooser.setFileHidingEnabled(true);

		chooser.showDialog(null, description);


		if (chooser.getSelectedFile() != null)
			return chooser.getSelectedFile();

		return null;
	}

}
