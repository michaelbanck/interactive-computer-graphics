package de.mb.helper;

import static org.lwjgl.glfw.GLFW.*;

import de.mb.datatypes.VectorImpl;
import de.mb.json.SceneLoader;
import de.mb.scenegraph.nodes.CamNode;
import de.mb.scenegraph.nodes.Node;
import ogl.app.Input;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Manipulators {

	private static long time = 0;

	private Manipulators() {
	}

	public static VectorImpl changeCoefficient(Input input, VectorImpl oldVector) {
		VectorImpl vecCof = oldVector;

		// diffus
		if (input.isKeyDown(GLFW_KEY_5)) {
			vecCof.x -= 0.01;
		}

		if (input.isKeyDown(GLFW_KEY_8)) {
			vecCof.x += 0.01;
		}

		// specular
		if (input.isKeyDown(GLFW_KEY_6)) {
			vecCof.y -= 0.01;
		}

		if (input.isKeyDown(GLFW_KEY_9)) {
			vecCof.y += 0.01;
		}

		// ambient
		if (input.isKeyDown(GLFW_KEY_7)) {
			vecCof.z -= 0.01;
		}

		if (input.isKeyDown(GLFW_KEY_0)) {
			vecCof.z += 0.01;
		}

		vecCof.x = checkValue(vecCof.x);
		vecCof.y = checkValue(vecCof.y);
		vecCof.z = checkValue(vecCof.z);

		return vecCof;
	}

	public static float changeCoefficient(Input input, float distance) {

		if (input.isKeyDown(GLFW_KEY_F11)) {
			distance += 0.01f;
			System.out.println("MAKE IT DARKER");

		}

		if (input.isKeyDown(GLFW_KEY_F12)) {
			distance -= 0.01f;
			System.out.println("MAKE IT BRIGHTER");
		}

		return checkValue(distance);
	}

	private static float checkValue(float a) {
		if (a > 2)
			return 2;
		if (a < 0)
			return 0;
		return a;
	}

	public static void nextCam(Input input) {

		if (input.isKeyDown(GLFW_KEY_F9))
			if (System.currentTimeMillis() - time > 500) {
				CamNode.setActive((CamNode.getActive()+1) % CamNode.getCam_count());
				
				time = System.currentTimeMillis();
			}
	}
	
	public static void saveScene(Input input, Node root){
		if(input.isKeyDown(GLFW_KEY_F10)){
			if(System.currentTimeMillis()-time > 1000){
				time = System.currentTimeMillis();
				System.out.println("SAVE");
				SceneLoader.write(root);
			}
		}
	}

}
