package de.mb.helper;

import de.mb.datatypes.MyMathFactory;
import de.mb.json.SceneLoader;
import de.mb.models.Model;
import de.mb.models.OBJLoader;
import de.mb.models.contants.Models;
import de.mb.models.contants.Textures;
import de.mb.scenegraph.actionnodes.CameraAction;
import de.mb.scenegraph.nodes.CamNode;
import de.mb.scenegraph.nodes.Node;
import de.mb.scenegraph.nodes.NodeBuilder;
import de.mb.sceneparts.BallGame;
import de.mb.sceneparts.Cube3Lights;
import de.mb.sceneparts.CubeTexturing;
import de.mb.sceneparts.JetGame;
import de.mb.sceneparts.SolarSystem;
import de.mb.sceneparts.StoneJumper;
import ogl.vecmath.Factory;
import ogl.vecmath.Matrix;

/**
 **********************************************************
 * Copyright (c) 2016 Michael Banck. All rights reserved. *
 **********************************************************
 *
 * @author Michael Banck 2004907
 *
 */
public class Szene {



	private Node root;

	Factory myFactory = MyMathFactory.vecmath;

	public Szene() {
		if (SceneLoader.loadDialog())// aus filesystem szene laden
			root = SceneLoader.read();
		else// szene aus javacode erzeugen
			initSzene();
	}

	private void initSzene() {

		Model modelGround = OBJLoader.loadModel(Models.GROUND);
		Matrix scaleGround = myFactory.scaleMatrix(45, 1, 45);

		NodeBuilder builder = NodeBuilder.getInstance();

		root = builder.getRoot();
		
		builder.createGeoNode(root, myFactory.translationMatrix(0, -2, 0).mult(scaleGround), modelGround).setTexturePath(Textures.GRAS);

	
		CamNode cam = builder.createCamNode(root);
		cam.addActionNode(new CameraAction());
		
	
		
		Node cubeTexturing = builder.createGroup(root, myFactory.translationMatrix(10,1,0).mult(myFactory.scaleMatrix(0.3f,0.3f,0.3f)));
		new CubeTexturing(cubeTexturing);
		
		
		Node ballGameNode = builder.createGroup(root, myFactory.translationMatrix(75,0,0));
		new BallGame(ballGameNode);
	
		Matrix solarMatrix = myFactory.translationMatrix(10,100,10).mult(myFactory.scaleMatrix(40,40,40));
		Node solarSystemNode = builder.createGroup(root, solarMatrix);
		SolarSystem.getSolarSystem(solarSystemNode);
		
		
		Node cubeLightRoot = builder.createGroup(root, myFactory.translationMatrix(-10,1,0).mult(myFactory.scaleMatrix(0.3f,0.3f,0.3f)));
		new Cube3Lights(cubeLightRoot);

		
//		the following nodes have no lights
//		builder.createLightNode(root, myFactory.translationMatrix(0,5,0));
		
		Node kooperStoneNode = builder.createGroup(root, myFactory.translationMatrix(5,0,15));
		new StoneJumper(kooperStoneNode);


		Node jetRootNode = builder.createGroup(root, myFactory.translationMatrix(0,1,0));
		new JetGame(jetRootNode);
		
		}
	
	public Node getRootNode(){
		return root;
	}
	


}
