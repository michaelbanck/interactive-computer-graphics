#version 140

in vec3 vertex;
in vec3 normal;
in vec2 texCoord;
out vec2 texCoord2;

uniform mat4  normalMatrix,  modelViewMatrix, projectionMatrix;

out vec3 vertPos;
out vec3 normalInterp;

void main() {

	texCoord2 = texCoord;
	vec4 vertPos4 =  modelViewMatrix*vec4(vertex, 1.0);
	gl_Position = projectionMatrix * vertPos4;
	vertPos = vec3(vertPos4) / vertPos4.w;
	normalInterp = vec3(normalMatrix * vec4(normal, 0.0));


}
