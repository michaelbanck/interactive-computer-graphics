#version 330

precision highp float;

uniform sampler2D tex_image;

in vec3 normalInterp;
in vec3 vertPos;

const vec3 lightPos = vec3(-20.0, -10.0, -10.0);
const vec3 ambientColor = vec3(0.5, 0.5, 0.5);
const vec3 diffuseColor = vec3(0.7, 0.0, 0.0);
const vec3 specColor = vec3(1.0, 1.0, 1.0);

in vec2 tex2d;

out vec4 frag_color;

void main(void)
{
  // retrieve texture color for fragment
  vec4 tex_color = texture(tex_image, tex2d.xy);

  vec3 normal = normalize(normalInterp);
  vec3 lightDir = normalize(lightPos - vertPos);

  float lambertian = max(dot(lightDir,normal), 0.0);
  float specular = 0.0;

  if(lambertian > 0.0) {

    vec3 viewDir = normalize(-vertPos);

      vec3 reflectDir = reflect(-lightDir, normal);
      float specAngle = max(dot(reflectDir, viewDir), 0.0);

      // note that the exponent is different here
      specular = pow(specAngle, 10.0);
  }

  frag_color = tex_color * vec4(ambientColor +
                      lambertian * diffuseColor +
                      specular * specColor, 1.0);
}";
