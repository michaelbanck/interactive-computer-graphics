#version 140

//this code is never used or tested...no devices with this GL Version....

attribute vec3 vertex;
attribute vec3 normal;
attribute vec2 texCoord;

varying vec2 texCoord2;
varying vec3 vertPos;
varying vec3 normalInterp;
uniform mat4  normalMatrix,  modelViewMatrix, projectionMatrix;

void main() {

	texCoord2 = texCoord;
	vec4 vertPos4 =  modelViewMatrix*vec4(vertex, 1.0);
	gl_Position = projectionMatrix * vertPos4;
	vertPos = vec3(vertPos4) / vertPos4.w;
	normalInterp = vec3(normalMatrix * vec4(normal, 0.0));
}
