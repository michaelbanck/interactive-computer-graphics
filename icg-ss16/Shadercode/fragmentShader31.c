#version 330
//this shader is tested with a notebook with Open GL 3.1, it works,
//but it can just use 16 images/texture, to see the whole scene in it perfect harmony, use a pc with Open GL 4.4
in vec2 texCoord2;
in vec3 normalInterp, vertPos;


uniform sampler2D tex, normalText, specularText;
uniform int lightCount, hasNormals, hasSpecular, isLight;
uniform float brightness;
uniform vec3[] lightArray;
uniform vec3 lightParts;
float ambCof = lightParts.z;
float specCof = lightParts.y;
float difCof = lightParts.x;


out vec4 fragColor;

vec3 ambient, ambientPart, difParts, specularParts, normal;
float shininess = 1000.0;
const int NOT_IN_USE = 16;


//vergleiche http://multivis.net/lecture/phong.html
vec3 getSpecular() {

	if (hasSpecular == NOT_IN_USE) {
		return vec3(1, 1, 1);//specular color
	}
		return vec3(texture(specularText, texCoord2)) * 2;

}

vec3 getNormal() {

	if (hasNormals == NOT_IN_USE) { //wenn keine normale
		return normalize(normalInterp);
	}

	shininess = 5;
	normal = vec3(texture(normalText, texCoord2));
	return normalize(normal * 2.0 - 1.0);

}

void getTextureStuff(){

	float aV =  0.3/lightCount;
	ambient = vec3(aV, aV, aV);
	normal = getNormal();

	vec3 textColor = vec3(texture(tex, texCoord2));
	vec3 specularMap = getSpecular();

	ambientPart = ambCof * ambient * textColor;
	specularParts = specCof  *  specularMap;
	difParts = difCof  *  textColor;

}

vec4 getColor(vec3 lightPosition) {


	vec3 lightDir = normalize(lightPosition - vertPos);
	float lambertian = max(dot(lightDir, normal), 0.0);
	float specular = 0.0;


	if (lambertian > 0.0) {
		vec3 viewDir = normalize(-vertPos);
		vec3 reflectDir = reflect(-lightDir, normal);
		float specAngle = max(dot(reflectDir, viewDir), 0.0);
		if(specAngle > 0.98)
			specular = pow(specAngle, shininess)*1.2;


	}


	float distance = length(lightPosition - vertPos);
	float disFactor = pow(1.0 / distance, brightness);


	vec3 colorLinear =
			(specularParts * specular
			+  difParts * lambertian) * disFactor
			+ ambientPart;

	return vec4(colorLinear, 1.0);

}

void main() {


	fragColor = vec4(0,0,0,1);

	if (isLight == 1) {//if geonode is light, they don't have shadows
		vec3 textColor = vec3(texture(tex, texCoord2)*3);
		fragColor = vec4(textColor, 1);
		return;
	}



	getTextureStuff();



	//damn hack, don't know how i can use the array with a non const variable....but it works...
	if (lightCount > 0)
		fragColor += getColor(lightArray[0]);
	else
		return;
	if (lightCount > 1)
		fragColor += getColor(lightArray[1]);
	else
		return;
	if (lightCount > 2)
		fragColor += getColor(lightArray[2]);
	else
		return;
	if (lightCount > 3)
		fragColor += getColor(lightArray[3]);
	else
		return;
	if (lightCount > 4)
		fragColor += getColor(lightArray[4]);
	else
		return;
	if (lightCount > 5)
		fragColor += getColor(lightArray[5]);
	else
		return;
	if (lightCount > 6)
		fragColor += getColor(lightArray[6]);
	else
		return;
	if (lightCount > 7)
		fragColor += getColor(lightArray[7]);
	else
		return;
	if (lightCount > 8)
		fragColor += getColor(lightArray[8]);
	else
		return;
	if (lightCount > 9)
		fragColor += getColor(lightArray[9]);
	else
		return;
	if (lightCount > 10)
		fragColor += getColor(lightArray[10]);
	else
		return;
	if (lightCount > 11)
		fragColor += getColor(lightArray[11]);
	else
		return;
	if (lightCount > 12)
		fragColor += getColor(lightArray[12]);
	else
		return;
	if (lightCount > 13)
		fragColor += getColor(lightArray[13]);
	else
		return;
	if (lightCount > 14)
		fragColor += getColor(lightArray[14]);
	else
		return;
	if (lightCount > 15)
		fragColor += getColor(lightArray[15]);
	else
		return;
	if (lightCount > 16)
		fragColor += getColor(lightArray[16]);
	else
		return;
	if (lightCount > 17)
		fragColor += getColor(lightArray[17]);
	else
		return;

}
